# weir-project--spring cloud 最新技术研究

#### 项目介绍
{spring cloud 最新技术研究。spring cloud 生态、阿里生态(Nacos dubbo rocketmq)、华为生态(Apache ServiceComb,Apache ServiceComb Saga),
最实战例子研究，分享实战经验，完全开源，少理论多实践，拿出代码说话，这就是该开源项目的目的，所有技术绝不会有任何形式的收费。}

#### 软件架构
目前有maven，jdk11，spring boot 2.2.5.RELEASE，spring cloud Hoxton.SR2，mysql8.x（更多技术请查看maven） 
例子有：
1. eureka-client（eureka客户端）
2. eureka-server（eureka服务端，注册中心）
3. gateway-zuul（zuul 网管）
4. oauth2（统一认证服务，初步）
5. parent（父工程）
6. permissions（权限资源，初步）
7. permissions-api（权限API）
8. spring-boot-admin（微服务监控）
9. spring-cloud-gateway（spring 网管 测试不通过）
10. zipkin-server（分布式跟踪系统）
11. spring-cloud-gateway2（spring 网管 测试通过）
12. seata 分布式事务demo
13. quarkus框架demo加入 （quarkus-spring-data-jpa-mysql）spring-data-jpa省了很多事儿
#### 安装教程

1. 不用多说，启动注册中心之后再启动各个服务
2. 关于数据库resources文件里面都有脚本

#### 使用说明

1. 欢迎大家提交代码，各分支说明技术点即可
2. 想成为开发者请点评留言申请（写清楚码云账号即可）
3. oauth2授权码模式步骤：
   
   3.1. 请求：http://localhost:8081/oauth/authorize?response_type=code&client_id=weir&redirect_uri=http://www.loveweir.com
       获得code
   
   3.2. 请求：http://localhost:8081/oauth/token?
        grant_type=authorization_code&code=EAV5A3&redirect_uri=http://www.loveweir.com&scope=all
        得到授权码：{
"access_token": "255862f2-15d7-4bf8-8750-8c686b11ea52",
"token_type": "bearer",
"expires_in": 7199,
"scope": "all"
}
   
   3.3. 利用授权码，放在请求的header里面请求资源。

#### 参与贡献

1. 任何人都可以参与
2. 每个人提交的代码必须是自己的分支
3. master合并分支必须在分支上经过测试
4. 任何人不得以任何借口做收费，所有分支代码全网共享


![输入图片说明](https://images.gitee.com/uploads/images/2020/0609/162237_f28f8509_2159.png "WechatIMG61.png")