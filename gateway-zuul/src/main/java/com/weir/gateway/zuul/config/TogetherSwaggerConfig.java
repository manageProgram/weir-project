package com.weir.gateway.zuul.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
@Component
@Primary
public class TogetherSwaggerConfig implements SwaggerResourcesProvider{

	private final RouteLocator routeLocator;
    public TogetherSwaggerConfig(RouteLocator routeLocator) {
		this.routeLocator = routeLocator;
	}
	@Override
	public List<SwaggerResource> get() {
		Set<SwaggerResource> resources = new TreeSet<SwaggerResource>();
		List<Route> routes = routeLocator.getRoutes();
		for (Route route : routes) {
			System.out.println("route----" + route.toString());
			String location = route.getFullPath().replace("**", "v2/api-docs");
			resources.add(swaggerResource(route.getId(), location, "1.0"));
		}
		return new ArrayList<SwaggerResource>(resources);
	}

	private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
