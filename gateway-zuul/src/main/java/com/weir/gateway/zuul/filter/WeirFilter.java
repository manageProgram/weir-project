package com.weir.gateway.zuul.filter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
@Component
public class WeirFilter extends ZuulFilter{

	/**
	 * shouldFilter：返回一个boolean类型来判断该过滤器是否要执行，
	 * 所以通过此函数可实现过滤器的开关。在上例中，我们直接返回true，所以该过滤器总是生效
	 */
	@Override
	public boolean shouldFilter() {
		RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        String uri = request.getRequestURI();
        if (uri.endsWith("v2/api-docs")) {
			return false;
		}
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext context = RequestContext.getCurrentContext();
		HttpServletRequest request = context.getRequest();
		String userToken = request.getParameter("userToken");
		if (StringUtils.isEmpty(userToken)) {
			context.setSendZuulResponse(false);
			context.setResponseBody("userToken is null");
			context.setResponseStatusCode(401);
			return null;
		}
		return null;
	}

	/**
	 * filterType：返回一个字符串代表过滤器的类型，在zuul中定义了四种不同生命周期的过滤器类型，具体如下：

pre：可以在请求被路由之前调用
route：在路由请求时候被调用
post：在route和error过滤器之后被调用
error：处理请求时发生错误时被调用
     Zuul的主要请求生命周期包括“pre”，“route”和“post”等阶段。对于每个请求，都会运行具有这些类型的所有过滤器。
	 */
	@Override
	public String filterType() {
		return "pre";
	}

	/**
	 * filterOrder：通过int值来定义过滤器的执行顺序
	 */
	@Override
	public int filterOrder() {
		return 0;
	}

}
