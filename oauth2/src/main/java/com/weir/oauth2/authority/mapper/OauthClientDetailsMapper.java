package com.weir.oauth2.authority.mapper;

import com.weir.oauth2.authority.entity.OauthClientDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {

}
