package com.weir.oauth2.authority.service;

import com.weir.oauth2.authority.entity.OauthClientDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
public interface IOauthClientDetailsService extends IService<OauthClientDetails> {

}
