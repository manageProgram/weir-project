package com.weir.oauth2.authority.service.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Service;

import com.weir.oauth2.feign.ClientDetailsClient;

@Service
public class ClientDetailsServiceImpl extends JdbcClientDetailsService {

	public ClientDetailsServiceImpl(DataSource dataSource) {
		super(dataSource);
	}

	@Autowired
	private ClientDetailsClient clientDetailsClient;
	
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
		BaseClientDetails clientDetails = clientDetailsClient.getClientInfo(clientId);
		return clientDetails;
	}
	
	public List<ClientDetails> listClientDetails() {
		return clientDetailsClient.listClientDetails();
	}
}