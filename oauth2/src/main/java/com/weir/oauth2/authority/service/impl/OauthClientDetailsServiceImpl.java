package com.weir.oauth2.authority.service.impl;

import com.weir.oauth2.authority.entity.OauthClientDetails;
import com.weir.oauth2.authority.mapper.OauthClientDetailsMapper;
import com.weir.oauth2.authority.service.IOauthClientDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
@Service
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsMapper, OauthClientDetails> implements IOauthClientDetailsService {

}
