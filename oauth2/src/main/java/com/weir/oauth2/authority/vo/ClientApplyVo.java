package com.weir.oauth2.authority.vo;

public class ClientApplyVo {

	private String clientId;
	private String clientSecret;
	private String webServerRedirectUri;
}
