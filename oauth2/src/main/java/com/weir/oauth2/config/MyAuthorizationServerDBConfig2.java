package com.weir.oauth2.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

// 配置授权中心信息
@Configuration
@EnableAuthorizationServer // 开启认证授权中心
@AutoConfigureAfter(AuthorizationServerEndpointsConfigurer.class)
public class MyAuthorizationServerDBConfig2 extends AuthorizationServerConfigurerAdapter {
	// accessToken有效期
	private int accessTokenValiditySeconds = 7200; // 两小时
	// 刷新accessToken有效期
	private int refreshTokenValiditySeconds = 7200; // 两小时

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Qualifier(value = "clientDetailsServiceImpl")
	private ClientDetailsService clientDetailsService;

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Bean
	public TokenStore tokenStore() {
		return new JdbcTokenStore(dataSource); /// 使用Jdbctoken store
	}
	/**
     * 授权store
     *
     * @return
     */
    @Bean
    public ApprovalStore approvalStore() {
        return new JdbcApprovalStore(dataSource);
    }
    /**
     * 授权码store
     *
     * @return
     */
    @Bean
    public AuthorizationCodeServices authorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(dataSource);
    }

	// 添加商户信息
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		// 申请获取appid和appkey
//		clients.jdbc(dataSource);
		clients.withClientDetails(clientDetailsService);
	}

	// 设置token类型
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
		.tokenStore(tokenStore())
		.approvalStore(approvalStore())
		.authenticationManager(authenticationManager)
		.authorizationCodeServices(authorizationCodeServices())
		.tokenServices(defaultTokenServices())
//		.userDetailsService(userDetailsService)
		.allowedTokenEndpointRequestMethods(HttpMethod.GET,
				HttpMethod.POST);
	}
	
	@Bean
    public DefaultTokenServices defaultTokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        // 是否支持刷新令牌
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setClientDetailsService(clientDetailsService);
        // token有效期自定义设置，默认12小时
        tokenServices.setAccessTokenValiditySeconds(accessTokenValiditySeconds);
        //默认30天，这里修改
        tokenServices.setRefreshTokenValiditySeconds(refreshTokenValiditySeconds);
        return tokenServices;
    }

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
		// 允许表单认证
		oauthServer.allowFormAuthenticationForClients();
		// 允许check_token访问
		oauthServer.checkTokenAccess("permitAll()");
	}

}
