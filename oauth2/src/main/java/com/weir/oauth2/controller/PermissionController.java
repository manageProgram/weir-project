package com.weir.oauth2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.weir.oauth2.feign.PermissionClient;

import io.swagger.annotations.Api;

@Api(tags = {"测试"})
@RequestMapping("/oauth2")
@RestController
public class PermissionController {

	@Autowired
	private PermissionClient permissionClient;
	
	@GetMapping("/getPermName/{userId}")
	public List<String> getPermName(@PathVariable("userId") Integer userId) {
		return permissionClient.getPermName(userId);
	}
	
	@GetMapping("/testOutTime")
	public String testOutTime() {
		return permissionClient.testOutTime();
	}
	//解决雪崩问题(默认开始服务隔离(线程池级别)、降级和熔断)
	/*hystrix:
		  command:
		    default:
		      execution:
		        timeout:
		          enabled: false*/
	@HystrixCommand(fallbackMethod = "testOutTimeHystrixFallback")
	@GetMapping("/testOutTimeHystrix")
	public String testOutTimeHystrix() {
		return permissionClient.testOutTime();
	}
	public String testOutTimeHystrixFallback() {
		return "testOutTimeHystrixFallback";
	}
	
	@GetMapping("/testOutTimeHystrix2")
	public String testOutTimeHystrix2() {
		return permissionClient.testOutTime();
	}
	
}
