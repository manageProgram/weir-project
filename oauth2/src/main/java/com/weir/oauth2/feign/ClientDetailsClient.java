package com.weir.oauth2.feign;

import com.weir.oauth2.feign.fallback.UserClientFallback;
import com.weir.permissions.api.IClientDetailsService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "permissions", fallback = UserClientFallback.class)
public interface ClientDetailsClient extends IClientDetailsService {
}
