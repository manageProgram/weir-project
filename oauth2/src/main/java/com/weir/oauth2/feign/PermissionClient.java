package com.weir.oauth2.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.weir.oauth2.feign.fallback.PermissionClientFallback;
import com.weir.permissions.api.IPermissionService;

@FeignClient(value = "permissions", fallback = PermissionClientFallback.class)
public interface PermissionClient extends IPermissionService{

}
