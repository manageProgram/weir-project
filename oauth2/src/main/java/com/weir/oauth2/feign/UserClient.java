package com.weir.oauth2.feign;

import com.weir.oauth2.feign.fallback.UserClientFallback;
import com.weir.permissions.api.IUserFeignService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "permissions", fallback = UserClientFallback.class)
public interface UserClient extends IUserFeignService {
}
