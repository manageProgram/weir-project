package com.weir.oauth2.feign.fallback;

import com.weir.oauth2.feign.ClientDetailsClient;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

@Component
public class ClientDetailsClientFallback implements ClientDetailsClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDetailsClientFallback.class);

	@Override
	public BaseClientDetails getClientInfo(String clientId) {
		return null;
	}

	@Override
	public List<ClientDetails> listClientDetails() {
		return null;
	}
}
