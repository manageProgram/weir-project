package com.weir.oauth2.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.weir.oauth2.feign.PermissionClient;
@Component
public class PermissionClientFallback implements PermissionClient {

	@Override
	public List<String> getRoleName(Integer userId) {
		return null;
	}

	@Override
	public List<String> getPermName(Integer userId) {
		return null;
	}

	@Override
	public String testOutTime() {
		return "testOutTimeHystrixFallback2";
	}

}
