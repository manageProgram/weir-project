package com.weir.oauth2.feign.fallback;

import com.weir.oauth2.feign.UserClient;
import com.weir.permissions.vo.UserInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserClientFallback implements UserClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserClientFallback.class);
    @Override
    public UserInfo getUser(String username) {
    	LOGGER.info("UserClientFallback----getUser----" + username);
        return null;
    }
}
