package com.weir.permissions.api;

import java.util.List;

import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IClientDetailsService {
	@GetMapping("/getClientInfo/{clientId}")
	BaseClientDetails getClientInfo(@PathVariable("clientId") String clientId);
	@GetMapping("/getAllClientInfos")
	List<ClientDetails> listClientDetails();

}
