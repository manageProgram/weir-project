package com.weir.permissions.api;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface IPermissionService {

	@GetMapping("/getRoleName/{userId}/")
	List<String> getRoleName(@PathVariable("userId") Integer userId);

	@GetMapping("/getPermName/{userId}/")
	List<String> getPermName(@PathVariable("userId") Integer userId);

	@GetMapping("/testOutTime")
	String testOutTime();

}
