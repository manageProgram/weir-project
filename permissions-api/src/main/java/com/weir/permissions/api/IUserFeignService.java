package com.weir.permissions.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.weir.permissions.vo.UserInfo;

public interface IUserFeignService {

	@GetMapping("/getUser/{username}/")
    public UserInfo getUser(@PathVariable("username") String username);
}
