//package com.weir.resources.permissions.config;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import com.weir.resources.permissions.entity.PermMenu;
//import com.weir.resources.permissions.mapper.PermMenuMapper;
//import com.weir.resources.permissions.security.WeirUserDetailsService;
//import com.weir.resources.permissions.vo.MD5Util;
//
//@Component
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled=true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Autowired
//	private WeirUserDetailsService weirUserDetailsService;
//	@Autowired
//	private PermMenuMapper permMenuMapper;
//
//	// 配置认证用户信息和权限
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(weirUserDetailsService).passwordEncoder(new PasswordEncoder() {
//
//			@Override
//			public boolean matches(CharSequence rawPassword, String encodedPassword) {
//				System.out.println("rawPassword-------"+rawPassword);
//				String encode = MD5Util.encode(rawPassword.toString());
//				boolean result = encodedPassword.trim().equals(encode);
//				return result;
//			}
//
//			@Override
//			public String encode(CharSequence rawPassword) {
//				return MD5Util.encode(rawPassword.toString());
//			}
//		});
//	}
//
//	// 配置拦截请求资源
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		List<PermMenu> permMenus = permMenuMapper.selectList(null);
//
//		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http
//				.authorizeRequests();
//
//		for (PermMenu permMenu : permMenus) {
//			if (!StringUtils.isEmpty(permMenu.getUrl()) && !StringUtils.isEmpty(permMenu.getCode())) {
//				authorizeRequests.antMatchers(permMenu.getUrl()).hasAnyAuthority(permMenu.getCode());
//			}
//		}
//		authorizeRequests.antMatchers("/login","/sr/**").permitAll().antMatchers("/**").fullyAuthenticated().and().formLogin()
//				.loginPage("/login").and().csrf().disable();
//	}
//	
////	@Override
////	public void configure(WebSecurity web) throws Exception {
//////		web.ignoring().antMatchers("/static/**");
////		web.ignoring().antMatchers("/**/*.js", "/lang/*.json", "/**/*.css", "/**/*.js", "/**/*.map", "/**/*.png");
////	}
//}
