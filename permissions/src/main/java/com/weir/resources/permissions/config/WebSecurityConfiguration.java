package com.weir.resources.permissions.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/", "/**/v2/api-docs/**", "/swagger-resources/**",
                "/swagger-ui.html", "/webjars/**", "/**/favicon.ico");
    }

}