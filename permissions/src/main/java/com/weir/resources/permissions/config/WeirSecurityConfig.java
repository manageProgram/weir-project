//package com.weir.resources.permissions.config;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//import org.springframework.util.StringUtils;
//
//import com.weir.resources.permissions.entity.PermMenu;
//import com.weir.resources.permissions.mapper.PermMenuMapper;
//import com.weir.resources.permissions.security.WeirUserDetailsService;
//import com.weir.resources.permissions.utils.MD5Util;
//
//@Component
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class WeirSecurityConfig extends WebSecurityConfigurerAdapter {
//	@Autowired
//	private WeirUserDetailsService weirUserDetailsService;
//	@Autowired
//	private PermMenuMapper permMenuMapper;
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////		auth.inMemoryAuthentication()
////		.passwordEncoder(passwordEncoder())
////		.withUser("weir").password(passwordEncoder().encode("123456")).authorities("admin");
//
//		auth.userDetailsService(weirUserDetailsService).passwordEncoder(new PasswordEncoder() {
//
//			@Override
//			public boolean matches(CharSequence rawPassword, String encodedPassword) {
//				return MD5Util.encode((String) rawPassword).equals(encodedPassword);
//			}
//
//			@Override
//			public String encode(CharSequence rawPassword) {
//				return MD5Util.encode((String) rawPassword);
//			}
//		});
//	}
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http.authorizeRequests();
//
//		authorizeRequests.antMatchers("/sr/**").permitAll();
//
//		List<PermMenu> permMenus = permMenuMapper.selectList(null);
//		for (PermMenu permMenu : permMenus) {
//			if (!StringUtils.isEmpty(permMenu.getUrl()) && !StringUtils.isEmpty(permMenu.getCode())) {
//				authorizeRequests.antMatchers(permMenu.getUrl()).hasAnyAuthority(permMenu.getCode());
//			}
//		}
//
//		authorizeRequests.antMatchers("/**").fullyAuthenticated()
//		.and().headers().frameOptions().disable()//x-frame-options deny
//		.and().formLogin()
//		.and().csrf().disable();
//
////		http.authorizeRequests().antMatchers("/sr/**").permitAll()
//////		.antMatchers("/welcome","/north","/south").hasAnyAuthority("admin")
////		.antMatchers("/**").fullyAuthenticated()
////		.and().headers().frameOptions().disable()//x-frame-options deny
////		.and().formLogin()
////		.and().csrf().disable();
//	}
//
//	@Bean
//	public BCryptPasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}
//}
