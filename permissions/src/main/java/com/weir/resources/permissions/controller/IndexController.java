package com.weir.resources.permissions.controller;

import java.util.Arrays;
import java.util.Map;

import javax.annotation.security.PermitAll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.weir.resources.permissions.entity.PermMenu;
import com.weir.resources.permissions.entity.PermRolePermission;
import com.weir.resources.permissions.service.IPermMenuService;
import com.weir.resources.permissions.service.IPermRolePermissionService;

import io.swagger.annotations.ApiOperation;

@Controller
public class IndexController {
	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@RequestMapping("/")
	public String main() {
		return "main";
	}

//	@PermitAll
	@RequestMapping("/welcome")
	public String welcome() {
		return "layout/welcome";
	}

//	@PermitAll
	@RequestMapping("/north")
	public String north() {
		return "layout/north";
	}

//	@PermitAll
	@RequestMapping("/south")
	public String south() {
		return "layout/south";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@Autowired
	private IPermMenuService permMenuService;
	@Autowired
	private IPermRolePermissionService rolePermissionService;

	@RequestMapping("/geturl")
	public void geturl() {
		Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping.getHandlerMethods();
		for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
			RequestMappingInfo info = m.getKey();
			HandlerMethod method = m.getValue();
			String url = info.getPatternsCondition().toString();
			System.out.println("url---------------" + url);
			url = url.replace("[", "");
			url = url.replace("]", "");
			String[] urls = url.split("/");
			System.out.println("urls------" + Arrays.deepToString(urls));

			ApiOperation apiOperation = method.getMethodAnnotation(ApiOperation.class);

			if (urls.length > 3) {
				String code = urls[2] + "_" + urls[3];
				int menuCount = permMenuService.count(new QueryWrapper<PermMenu>().eq("code", code));
				if (menuCount == 0) {
					PermMenu permMenu = permMenuService.getOne(new QueryWrapper<PermMenu>().eq("code", urls[2]));
					if (apiOperation != null && permMenu != null) {
						String value = apiOperation.value();
						System.out.println("apiOperation---------" + value);
						PermMenu menu = new PermMenu();
						menu.setCode(code);
						menu.setPid(permMenu.getId());
						menu.setStep(2);
						menu.setText(apiOperation.value());
						menu.setUrl(url);
						permMenuService.save(menu);
						rolePermissionService.save(new PermRolePermission(menu.getId(),1));
					}
				}
			}

		}
	}
}
