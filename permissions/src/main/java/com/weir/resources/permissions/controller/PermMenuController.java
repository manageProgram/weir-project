package com.weir.resources.permissions.controller;


import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.weir.resources.permissions.entity.PermMenu;
import com.weir.resources.permissions.entity.PermUser;
import com.weir.resources.permissions.service.IPermMenuService;
import com.weir.resources.permissions.utils.BaseUtil;
import com.weir.resources.permissions.utils.JsonUtil;
import com.weir.resources.permissions.vo.Json;
import com.weir.resources.permissions.vo.MenuVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * <p>
 * 菜单 前端控制器
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
@Api(tags = { "菜单和权限管理" })
@Controller
@RequestMapping("/perm/menu")
public class PermMenuController {
	@Autowired
	private IPermMenuService permMenuService;

//	@Perm(privilegeValue = "module_listUI",name="菜单列表UI")
	@ApiOperation("菜单权限列表")
	@GetMapping("/list")
	public String list() {
		return "base/admin/module";
	}
	@GetMapping("/tree/{user_id}")
	@ResponseBody
	public List<MenuVo> tree(Integer id,
			@PathVariable("user_id") Integer userId) {
		return permMenuService.getTrees(id,userId);
	}
	
	@PreAuthorize("hasAuthority('menu_tree')")
	@GetMapping("/tree")
	@ResponseBody
	public List<MenuVo> listTree() {
		return permMenuService.listTree();
	}
	
//	@Perm(privilegeValue = "module_addUI",name="菜单添加UI")
	@GetMapping("/add")
	public String addUI(Model model) {
		model.addAttribute("menu", new PermMenu());
		return "base/admin/module_add";
	}
//	@MethodLog(description="菜单添加")
	@ApiOperation("菜单权限添加/修改")
	@PostMapping("/add")
	@ResponseBody
//	@Perm(privilegeValue = "module_add",name="菜单添加")
	public Json add(PermMenu module) {
		if (!BaseUtil.isEmpty(module.getText())) {
			return JsonUtil.jsonError("名称不能为空");
		}
		if (!BaseUtil.isEmpty(module.getStep())) {
			return JsonUtil.jsonError("级别不能为空");
		}
		
		try {
			permMenuService.saveOrUpdate(module);
			return JsonUtil.jsonSuccess("菜单添加或修改成功");
		} catch (Exception e) {
			return JsonUtil.jsonError("菜单添加或修改失败");
		}
	}
	
//	@Perm(privilegeValue = "module_editUI",name="菜单修改UI")
	@GetMapping("/edit")
	public String editUI(Integer id,Model model) {
		model.addAttribute("menu", permMenuService.getById(id));
		return "base/admin/module_add";
	}
//	@MethodLog(description="菜单删除")
	@ApiOperation("菜单权限删除")
	@PostMapping("/delete")
	@ResponseBody
//	@Perm(privilegeValue = "module_delete",name="菜单删除")
	public Json delete(Integer id) {
		try {
			permMenuService.delete(id);
			return JsonUtil.jsonSuccess("菜单删除成功");
		} catch (Exception e) {
			return JsonUtil.jsonError("菜单删除失败");
		}
	}
}

