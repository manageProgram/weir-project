package com.weir.resources.permissions.controller;


import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weir.resources.permissions.entity.PermRole;
import com.weir.resources.permissions.entity.PermRolePermission;
import com.weir.resources.permissions.service.IPermRolePermissionService;
import com.weir.resources.permissions.service.IPermRoleService;
import com.weir.resources.permissions.utils.BaseUtil;
import com.weir.resources.permissions.utils.DataGrid;
import com.weir.resources.permissions.utils.JsonUtil;
import com.weir.resources.permissions.vo.Json;
import com.weir.resources.permissions.vo.PermRoleVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Api(tags = { "角色管理" })
@Controller
@RequestMapping("/perm/role")
public class PermRoleController {
	
	@Autowired
	private IPermRoleService permRoleService;
	@Autowired
	private IPermRolePermissionService permRolePermissionService;
//	@Perm(privilegeValue = "role_listUI",name="角色列表UI")
	@ApiOperation("角色分页列表UI")
	@GetMapping("/list_ui")
	public String list() {
		return "base/admin/role";
	}
	
	@ApiOperation("角色分页列表")
	@GetMapping("/list")
	@ResponseBody
	public DataGrid<PermRole> list(@RequestParam(value = "current",defaultValue="1") Integer current,
			@RequestParam(value = "size",defaultValue="10") Integer size) {
		IPage<PermRole> page = permRoleService.page(new Page<PermRole>(current, size), new QueryWrapper<PermRole>());
		DataGrid<PermRole> dataGrid = new DataGrid<>(page.getTotal(), page.getRecords());
		return dataGrid;
	}
	
	@ApiOperation("获取角色全部数据")
	@GetMapping("/get_all")
	@ResponseBody
	public List<PermRole> getAll() {
		return permRoleService.list();
	}
	
//	@Perm(privilegeValue = "role_addUI",name="角色添加UI")
	@GetMapping("/add")
	public String addUI(Model model) {
		model.addAttribute("role", new PermRoleVo());
		return "base/admin/role_add";
	}
//	@MethodLog(description="角色添加")
//	@Perm(privilegeValue = "role_add",name="角色添加")
	@PostMapping("/add")
	@ResponseBody
	public Json add(PermRoleVo role) {
		if (!BaseUtil.isEmpty(role.getName())) {
			return JsonUtil.jsonError("角色名不能为空");
		}
		try {
			permRoleService.insertOrUpdate(role);
			return JsonUtil.jsonSuccess("角色添加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.jsonError("角色添加失败");
		}
	}
//	@Perm(privilegeValue = "role_editUI",name="角色修改UI")
	@GetMapping("/edit")
	public String editUI(Integer id,Model model) {
		if (BaseUtil.isEmpty(id)) {
			PermRole role = permRoleService.getById(id);
			List<PermRolePermission> rolemodules = permRolePermissionService.list(
					new QueryWrapper<PermRolePermission>().eq("role_id", id).isNotNull("permission_id"));
			List<Integer> ids = new ArrayList<>();
			for (PermRolePermission rolemodule : rolemodules) {
				ids.add(rolemodule.getPermissionId());
			}
			PermRoleVo roleVo = new PermRoleVo();
			BeanUtils.copyProperties(role, roleVo);
			roleVo.setPermissionIds(StringUtils.collectionToCommaDelimitedString(ids));
			model.addAttribute("role", roleVo);
		}
		return "base/admin/role_add";
	}
//	@MethodLog(description="角色删除")
//	@Perm(privilegeValue = "role_delete",name="角色删除")
	@PostMapping("/delete")
	@ResponseBody
	public Json delete(Integer id) {
		try {
			permRoleService.del(id);
			return JsonUtil.jsonSuccess("角色删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.jsonError("角色删除失败");
		}
	}
}

