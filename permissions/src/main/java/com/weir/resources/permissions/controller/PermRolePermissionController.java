package com.weir.resources.permissions.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 角色权限表 前端控制器
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Controller
@RequestMapping("/permissions/permRolePermission")
public class PermRolePermissionController {

}

