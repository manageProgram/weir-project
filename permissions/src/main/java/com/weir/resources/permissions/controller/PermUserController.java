package com.weir.resources.permissions.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weir.resources.permissions.entity.PermUser;
import com.weir.resources.permissions.service.IPermUserService;
import com.weir.resources.permissions.utils.BaseUtil;
import com.weir.resources.permissions.utils.DataGrid;
import com.weir.resources.permissions.utils.JsonUtil;
import com.weir.resources.permissions.utils.MD5Util;
import com.weir.resources.permissions.vo.Json;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Api(tags = { "用户管理" })
@Controller
@RequestMapping("/perm/user")
public class PermUserController {

	@Autowired
	private IPermUserService permUserService;
	
	@ApiOperation("用户列表UI")
	@GetMapping("/list_ui")
	public String list() {
		return "base/admin/user";
	}

	@PreAuthorize("hasAuthority('user_list')")
	@ApiOperation("用户列表")
	@GetMapping("/list")
	@ResponseBody
	public DataGrid<PermUser> list(@RequestParam(value = "current",defaultValue="1") Integer current,
			@RequestParam(value = "size",defaultValue="10") Integer size) {
		IPage<PermUser> page = permUserService.page(new Page<PermUser>(current, size), new QueryWrapper<>());
		DataGrid<PermUser> dataGrid = new DataGrid<>(page.getTotal(), page.getRecords());
		return dataGrid;
	}
	
	@GetMapping("/add")
	public String addUI() {
		return "base/admin/user_add";
	}
	
	@ApiOperation("用户添加")
	@PostMapping("/add")
	@ResponseBody
	public Json add(PermUser users) {
		if (!BaseUtil.isEmpty(users.getUsername())) {
			return JsonUtil.jsonError("用户名不能为空");
		}
		if (permUserService.exitUserByField("username", users.getUsername().trim())) {
			return JsonUtil.jsonError("该用户名已经存在");
		}
		if (!BaseUtil.isEmpty(users.getPwd())) {
			return JsonUtil.jsonError("密码不能为空");
		}
		users.setPwd(MD5Util.encode(users.getPwd()));
		
		try {
			permUserService.save(users);
			return JsonUtil.jsonSuccess("用户添加成功");
		} catch (Exception e) {
			return JsonUtil.jsonError("用户添加失败");
		}
	}
	
	@GetMapping("/edit/{id}")
	public String editUI(@PathVariable String id, Model model) {
		PermUser users = permUserService.getById(id);
		model.addAttribute("user", users);
		return "base/admin/user_edit";
	}
	@ApiOperation("用户修改")
	@PostMapping("/edit")
	@ResponseBody
	public Json edit(PermUser users) {
		if (!BaseUtil.isEmpty(users.getId())) {
			return JsonUtil.jsonError("用户ID没有获取到");
		}
		if (!BaseUtil.isEmpty(users.getUsername())) {
			return JsonUtil.jsonError("用户名不能为空");
		}
		if (!BaseUtil.isEmpty(users.getPwd())) {
			return JsonUtil.jsonError("密码不能为空");
		}
		users.setPwd(MD5Util.encode(users.getPwd()));
		
		try {
			permUserService.updateById(users);
			return JsonUtil.jsonSuccess("用户修改成功");
		} catch (Exception e) {
			return JsonUtil.jsonError("用户修改失败");
		}
	}

	/*@ApiOperation("获取用户的角色")
	@GetMapping("/getRoleName/{userId}/")
	public List<String> getRoleName(@PathVariable("userId") Integer userId) {
		return permUserService.getRoleName(userId);
	}

	@ApiOperation("获取用户的权限")
	@GetMapping("/getPermName/{userId}/")
	public List<String> getPermName(@PathVariable("userId") Integer userId) {
		return permUserService.getPermName(userId);
	}*/
}
