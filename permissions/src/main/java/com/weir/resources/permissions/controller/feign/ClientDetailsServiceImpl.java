package com.weir.resources.permissions.controller.feign;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.weir.permissions.api.IClientDetailsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(tags = { "第三方应用Feign管理" })
@RestController
public class ClientDetailsServiceImpl implements IClientDetailsService{
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientDetailsServiceImpl.class);
	@Autowired
    private JdbcClientDetailsService jdbcClientDetailsService;
	
	@ApiOperation("根据clientId获取第三方应用信息")
	@GetMapping("/getClientInfo/{clientId}")
	@Override
	public BaseClientDetails getClientInfo(@PathVariable("clientId") String clientId) {
		BaseClientDetails clientDetails = (BaseClientDetails) jdbcClientDetailsService.loadClientByClientId(clientId);
		clientDetails.setAuthorities(null);
		LOGGER.debug("ClientDetailsServiceImpl-------" + clientDetails.toString());
		return clientDetails;
	}
	
	@ApiOperation("获取所有第三方应用信息")
	@GetMapping("/getAllClientInfos")
	@Override
    public List<ClientDetails> listClientDetails() {
		List<ClientDetails> listClientDetails = jdbcClientDetailsService.listClientDetails();
		return listClientDetails;
	}
}
