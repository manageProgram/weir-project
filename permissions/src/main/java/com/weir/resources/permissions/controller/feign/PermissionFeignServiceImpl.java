package com.weir.resources.permissions.controller.feign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.weir.permissions.api.IPermissionService;
import com.weir.resources.permissions.service.IPermUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = { "测试Feign管理" })
@RestController
public class PermissionFeignServiceImpl implements IPermissionService {

	@Autowired
	private IPermUserService permUserService;
	
	@ApiOperation("获取用户的角色")
	@GetMapping("/getRoleName/{userId}/")
	@Override
	public List<String> getRoleName(@PathVariable("userId") Integer userId) {
		return permUserService.getRoleName(userId);
	}

	@ApiOperation("获取用户的权限")
	@GetMapping("/getPermName/{userId}/")
	@Override
	public List<String> getPermName(@PathVariable("userId") Integer userId) {
		return permUserService.getPermName(userId);
	}
	//Read timed out executing GET http://permissions/testOutTime
	//在oauth2 配置文件中：
	/*feign:
		  client:
		    config:
		      default:
		        connect-timeout: 5000
		        read-timeout: 5000*/
	@Override
	@ApiOperation("测试超时")
	@GetMapping("/testOutTime")
	public String testOutTime() {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "testOutTime";
	}
}
