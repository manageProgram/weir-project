package com.weir.resources.permissions.controller.oauth2;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weir.resources.permissions.entity.oauth2.OauthClientDetails;
import com.weir.resources.permissions.service.oauth2.IOauthClientDetailsService;
import com.weir.resources.permissions.utils.DataGrid;
import com.weir.resources.permissions.utils.JsonUtil;
import com.weir.resources.permissions.vo.Json;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

/**
 * <p>
 *  第三方应用接入管理
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
@Api(tags = { "第三方应用接入管理" })
@Controller
@RequestMapping("/oauth2/client")
public class OauthClientDetailsController {

	@Autowired
	private IOauthClientDetailsService oauthClientDetailsService;
	
	@ApiOperation("应用分页列表UI")
	@GetMapping("/list_ui")
	public String list() {
		return "/base/oauth2/client/list";
	}
	
	@ApiOperation("应用分页列表")
	@GetMapping("/list")
	@ResponseBody
	public DataGrid<OauthClientDetails> page(@RequestParam(value = "current",defaultValue="1") Integer current,
			@RequestParam(value = "size",defaultValue="10") Integer size) {
		IPage<OauthClientDetails> page = oauthClientDetailsService.page(new Page<OauthClientDetails>(current, size), new QueryWrapper<>());
		DataGrid<OauthClientDetails> dataGrid = new DataGrid<>(page.getTotal(), page.getRecords());
		return dataGrid;
	}
	
	@ApiOperation("应用添加UI")
	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("clientDetails", new OauthClientDetails());
		return "/base/oauth2/client/add";
	}
	
	@ApiOperation("应用添加")
	@PostMapping("/add")
	@ResponseBody
	public Json add(OauthClientDetails clientDetails) {
		try {
			oauthClientDetailsService.saveOrUpdate(clientDetails);
			return JsonUtil.jsonSuccess("添加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.jsonError("添加失败");
		}
	}
}

