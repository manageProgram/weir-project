package com.weir.resources.permissions.disruptor;

public class EventFactory implements com.lmax.disruptor.EventFactory<SeriesDataEvent> {

	public SeriesDataEvent newInstance() {
		return new SeriesDataEvent();
	}
}
