package com.weir.resources.permissions.disruptor;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.WaitStrategy;
import com.lmax.disruptor.WorkHandler;

@Component
public class SeriesDataEventQueueHelper extends BaseQueueHelper<SeriesData, SeriesDataEvent, SeriesDataEventHandler> implements InitializingBean {
 
    private static final int QUEUE_SIZE = 1024;
 
    @Autowired
    private List<SeriesDataEventHandler> seriesDataEventHandler;
 
 
    @Override
    protected int getQueueSize() {
        return QUEUE_SIZE;
    }
 
    @Override
    protected EventFactory eventFactory() {
        return new EventFactory();
    }
 
    @Override
    protected WorkHandler[] getHandler() {
        int size = seriesDataEventHandler.size();
        SeriesDataEventHandler[] paramEventHandlers = (SeriesDataEventHandler[]) seriesDataEventHandler.toArray(new SeriesDataEventHandler[size]);
        return paramEventHandlers;
    }
 
    @Override
    protected WaitStrategy getStrategy() {
        return new BlockingWaitStrategy();
        //return new YieldingWaitStrategy();
    }
 
    @Override
    public void afterPropertiesSet() throws Exception {
        this.init();
    }
}
