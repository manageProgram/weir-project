package com.weir.resources.permissions.mapper;

import com.weir.resources.permissions.entity.PermRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermRolePermissionMapper extends BaseMapper<PermRolePermission> {

}
