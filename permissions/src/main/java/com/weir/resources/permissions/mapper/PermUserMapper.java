package com.weir.resources.permissions.mapper;

import com.weir.resources.permissions.entity.PermUser;

import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermUserMapper extends BaseMapper<PermUser> {

	Set<String> selectRoleNameByUserId(@Param("userId") Integer userId);
	
	Set<String> selectPermNameByUserId(@Param("userId") Integer userId);
}
