package com.weir.resources.permissions.mapper;

import com.weir.resources.permissions.entity.PermUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface PermUserRoleMapper extends BaseMapper<PermUserRole> {

}
