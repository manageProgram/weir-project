package com.weir.resources.permissions.mapper.oauth2;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.resources.permissions.entity.oauth2.OauthClientDetails;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
public interface OauthClientDetailsMapper extends BaseMapper<OauthClientDetails> {

}
