package com.weir.resources.permissions.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.weir.resources.permissions.utils.JsonUtil;
import com.weir.resources.permissions.vo.Json;

import org.springframework.web.bind.annotation.ExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Json MethodArgumentNotValidHandler(HttpServletRequest request, MethodArgumentNotValidException exception) throws Exception {
        Json json = JsonUtil.jsonError("参数未通过验证异常",exception.getBindingResult().getFieldError().getDefaultMessage());
        return json;
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Json HttpMessageNotReadableHandler(HttpServletRequest request, HttpMessageNotReadableException exception) throws Exception {
        logger.info(exception.getMessage());
        Json json = JsonUtil.jsonError("参数无法正常解析");
        return json;
    }

	/*
	 * @ExceptionHandler(value = ExpiredJwtException.class) public Object
	 * ExpiredJwtExceptionHandler(ExpiredJwtException exception) throws Exception {
	 * logger.info(exception.getMessage()); ResultObject resultMsg =
	 * ResultObject.dataMsg("登录已过期！", StatusCode.FORBIDDEN); return resultMsg; }
	 */

    @ExceptionHandler(value = AccessDeniedException.class)
    public Json AccessDeniedExceptionHandler(AccessDeniedException exception) throws Exception {
        logger.info(exception.getMessage());
        Json json = JsonUtil.jsonError("权限不足！");
        return json;
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public Json NoHandlerFoundExceptionHandler(NoHandlerFoundException exception) throws Exception {
        logger.info(exception.getMessage());
        return JsonUtil.jsonError("链接不存在");
    }
    /**
     * 处理自定义异常
     */
	/*
	 * @ExceptionHandler(value = WelendException.class) public Object
	 * WelendExceptionHandler(WelendException e) { ResultObject r = new
	 * ResultObject(); r.setStatus(String.valueOf(e.getCode()));
	 * r.setMessage(e.getMessage()); return r; }
	 */

    @ExceptionHandler(value = AuthenticationException.class)
    public Json AuthenticationExceptionHandler(AuthenticationException e) {
        return JsonUtil.jsonError(e.getLocalizedMessage());
    }

    @ExceptionHandler(value = DuplicateKeyException.class)
    public Json DuplicateKeyExceptionHandler(DuplicateKeyException e) throws Exception {
        logger.error(e.getMessage(), e);
        return JsonUtil.jsonError(e.getMessage());
    }

    @ExceptionHandler(value = BadCredentialsException.class)
    public Json BadCredentialsExceptionHandler(BadCredentialsException e) throws Exception {
        logger.error(e.getMessage(), e);
        return JsonUtil.jsonError(e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Json ExceptionHandler(Exception e) throws Exception {
        logger.error(e.getMessage(), e);
        return JsonUtil.jsonError(e.getMessage());
    }
}