//package com.weir.resources.permissions.security;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//public class User implements UserDetails {
//
//	private static final long serialVersionUID = -5592964930950497361L;
//	private Integer id;
//	private String username;
//	private String realname = "weir";
//	private String password;
//	private Date createDate = new Date();
//	private Date lastLoginTime = new Date();
//	private boolean enabled = true;
//	private boolean accountNonExpired = true;
//	private boolean accountNonLocked = true;
//	private boolean credentialsNonExpired = true;
//	// 用户所有权限
//	private List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return authorities;
//	}
//
//	public Integer getId() {
//		return id;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	public String getRealname() {
//		return realname;
//	}
//
//	public void setRealname(String realname) {
//		this.realname = realname;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public Date getCreateDate() {
//		return createDate;
//	}
//
//	public void setCreateDate(Date createDate) {
//		this.createDate = createDate;
//	}
//
//	public Date getLastLoginTime() {
//		return lastLoginTime;
//	}
//
//	public void setLastLoginTime(Date lastLoginTime) {
//		this.lastLoginTime = lastLoginTime;
//	}
//
//	public boolean isEnabled() {
//		return enabled;
//	}
//
//	public void setEnabled(boolean enabled) {
//		this.enabled = enabled;
//	}
//
//	public boolean isAccountNonExpired() {
//		return accountNonExpired;
//	}
//
//	public void setAccountNonExpired(boolean accountNonExpired) {
//		this.accountNonExpired = accountNonExpired;
//	}
//
//	public boolean isAccountNonLocked() {
//		return accountNonLocked;
//	}
//
//	public void setAccountNonLocked(boolean accountNonLocked) {
//		this.accountNonLocked = accountNonLocked;
//	}
//
//	public boolean isCredentialsNonExpired() {
//		return credentialsNonExpired;
//	}
//
//	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
//		this.credentialsNonExpired = credentialsNonExpired;
//	}
//
//	public void setAuthorities(List<GrantedAuthority> authorities) {
//		this.authorities = authorities;
//	}
//
//}