package com.weir.resources.permissions.service;

import com.weir.resources.permissions.entity.PermMenu;
import com.weir.resources.permissions.vo.MenuVo;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-24
 */
public interface IPermMenuService extends IService<PermMenu> {

	List<PermMenu> getModule(Wrapper<PermMenu> wrapper);

	void delete(Integer id);

	List<MenuVo> listTree();

	List<MenuVo> getTrees(Integer id, Integer userId);

}
