package com.weir.resources.permissions.service;

import com.weir.resources.permissions.entity.PermRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermRolePermissionService extends IService<PermRolePermission> {

}
