package com.weir.resources.permissions.service;

import com.weir.resources.permissions.entity.PermRole;
import com.weir.resources.permissions.vo.PermRoleVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermRoleService extends IService<PermRole> {

	void del(Integer id);

	void insertOrUpdate(PermRoleVo role);

}
