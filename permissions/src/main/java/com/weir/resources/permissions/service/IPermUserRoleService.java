package com.weir.resources.permissions.service;

import com.weir.resources.permissions.entity.PermUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermUserRoleService extends IService<PermUserRole> {

}
