package com.weir.resources.permissions.service;

import com.weir.resources.permissions.entity.PermUser;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
public interface IPermUserService extends IService<PermUser> {

	List<String> getRoleName(Integer userId);

	List<String> getPermName(Integer userId);

	boolean exitUserByField(String field, String name);

}
