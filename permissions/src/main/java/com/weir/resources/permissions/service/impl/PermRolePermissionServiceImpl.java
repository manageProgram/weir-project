package com.weir.resources.permissions.service.impl;

import com.weir.resources.permissions.entity.PermRolePermission;
import com.weir.resources.permissions.mapper.PermRolePermissionMapper;
import com.weir.resources.permissions.service.IPermRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRolePermissionServiceImpl extends ServiceImpl<PermRolePermissionMapper, PermRolePermission> implements IPermRolePermissionService {

}
