package com.weir.resources.permissions.service.impl;

import com.weir.resources.permissions.entity.PermRole;
import com.weir.resources.permissions.entity.PermRolePermission;
import com.weir.resources.permissions.mapper.PermRoleMapper;
import com.weir.resources.permissions.mapper.PermRolePermissionMapper;
import com.weir.resources.permissions.service.IPermRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.weir.resources.permissions.utils.BaseUtil;
import com.weir.resources.permissions.vo.PermRoleVo;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermRoleServiceImpl extends ServiceImpl<PermRoleMapper, PermRole> implements IPermRoleService {
	@Autowired
	private PermRolePermissionMapper permRolePermissionMapper;
	@Autowired
	private PermRoleMapper permRoleMapper;

	@Override
	@Transactional
	public void del(Integer id) {
		permRolePermissionMapper.delete(new QueryWrapper<PermRolePermission>().eq("role_id", id));
		permRoleMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void insertOrUpdate(PermRoleVo role) {
		if (BaseUtil.isEmpty(role.getId())) {
			permRolePermissionMapper.delete(new QueryWrapper<PermRolePermission>().eq("role_id", role.getId()));
		}
		PermRole permRole = new PermRole();
		BeanUtils.copyProperties(role, permRole);
		saveOrUpdate(permRole);
		if (BaseUtil.isEmpty(role.getPermissionIds())) {
			String[] ids = role.getPermissionIds().split(",");
			for (String s : ids) {
				PermRolePermission rolemodule = new PermRolePermission(Integer.valueOf(s), permRole.getId());
				permRolePermissionMapper.insert(rolemodule);
			}

			/*
			 * if (BaseUtil.isEmpty(role.getParentPermissionIds())) { String[] ids2 =
			 * role.getParentPermissionIds().split(","); for (String s : ids2) {
			 * PermRolePermission rolemodule = new PermRolePermission();
			 * rolemodule.setRoleId(role.getId());
			 * rolemodule.setParentModuleid(Integer.valueOf(s));
			 * aotemaiRolemoduleService.insert(rolemodule); } }
			 */

		}
	}
}
