package com.weir.resources.permissions.service.impl;

import com.weir.resources.permissions.entity.PermUserRole;
import com.weir.resources.permissions.mapper.PermUserRoleMapper;
import com.weir.resources.permissions.service.IPermUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermUserRoleServiceImpl extends ServiceImpl<PermUserRoleMapper, PermUserRole> implements IPermUserRoleService {

}
