package com.weir.resources.permissions.service.impl;

import com.weir.resources.permissions.entity.PermUser;
import com.weir.resources.permissions.mapper.PermUserMapper;
import com.weir.resources.permissions.service.IPermUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-01
 */
@Service
public class PermUserServiceImpl extends ServiceImpl<PermUserMapper, PermUser> implements IPermUserService {

	@Autowired
	private PermUserMapper permUserMapper;
	
	@Override
	public List<String> getRoleName(Integer userId) {
		return new ArrayList<String>(permUserMapper.selectRoleNameByUserId(userId));
	}
	
	@Override
	public List<String> getPermName(Integer userId) {
		return new ArrayList<String>(permUserMapper.selectPermNameByUserId(userId));
	}
	
	@Override
	public boolean exitUserByField(String field,String name) {
		if (permUserMapper.selectCount(new QueryWrapper<PermUser>().eq(field, name))>0) {
			return true;
		}
		return false;
	}
}
