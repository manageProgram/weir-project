package com.weir.resources.permissions.service.impl.oauth2;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.resources.permissions.entity.oauth2.OauthClientDetails;
import com.weir.resources.permissions.mapper.oauth2.OauthClientDetailsMapper;
import com.weir.resources.permissions.service.oauth2.IOauthClientDetailsService;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
@Service
public class OauthClientDetailsServiceImpl extends ServiceImpl<OauthClientDetailsMapper, OauthClientDetails> implements IOauthClientDetailsService {

}
