package com.weir.resources.permissions.service.oauth2;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.resources.permissions.entity.oauth2.OauthClientDetails;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2018-12-10
 */
public interface IOauthClientDetailsService extends IService<OauthClientDetails> {

}
