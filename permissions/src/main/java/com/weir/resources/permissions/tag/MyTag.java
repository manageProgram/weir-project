package com.weir.resources.permissions.tag;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

public class MyTag extends AbstractProcessorDialect  {

	private static final String DIALECT_NAME = "weir";
	 
	private static final String PREFIX = "weir";
 
	public MyTag() {
		super(DIALECT_NAME, PREFIX, StandardDialect.PROCESSOR_PRECEDENCE);
	}
 
	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processors = new HashSet<IProcessor>();
		// <iftg:select/> 注解
		processors.add(new PermissionTag(dialectPrefix));
		// This will remove the xmlns:score attributes we might add for IDE validation
		processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
 
		return processors;
	}

}
