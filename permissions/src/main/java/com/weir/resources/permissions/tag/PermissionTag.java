package com.weir.resources.permissions.tag;

import org.springframework.context.ApplicationContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

public class PermissionTag extends AbstractElementTagProcessor {

	private static final String TAG_NAME = "permission";// 标签名
	private static final int PRECEDENCE = 10000;// 优先级

	public PermissionTag(String dialectPrefix) {
		super(TemplateMode.HTML, // 此处理器将仅应用于HTML模式
				dialectPrefix, // 要应用于名称的匹配前缀
				TAG_NAME, // 标签名称：匹配此名称的特定标签
				true, // 将标签前缀应用于标签名称
				null, // 无属性名称：将通过标签名称匹配
				false, // 没有要应用于属性名称的前缀
				PRECEDENCE);// 优先(内部方言自己的优先)
	}

	@Override
	protected void doProcess(ITemplateContext context, IProcessableElementTag tag,
			IElementTagStructureHandler structureHandler) {
		ApplicationContext appCtx = SpringContextUtils.getApplicationContext(context);
//		MatterMapper mapper = appCtx.getBean(MatterMapper.class);
		final String matterid = tag.getAttributeValue("matterid");
	}

}
