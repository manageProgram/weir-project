package com.weir.resources.permissions.utils;

import java.util.ArrayList;
import java.util.List;
/**
 * 
* @ClassName: DataGrid 
* @Description: easyui数据封装
* @author weir
* @date 2016年11月10日 下午2:32:25 
* 
* @param <T>
 */
public class DataGrid<T> {

	private long total = 0;
	private List<T> rows = new ArrayList<T>();

	public DataGrid() {
		super();
	}

	public DataGrid(long total, List<T> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

}
