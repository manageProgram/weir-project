/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3308
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3308
 Source Schema         : weir_permissions

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 29/03/2019 09:43:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for perm_menu
-- ----------------------------
DROP TABLE IF EXISTS `perm_menu`;
CREATE TABLE `perm_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块名称',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块编码',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模块路径',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父级ID',
  `step` int(1) NULL DEFAULT NULL COMMENT '级别(1菜单和2功能)',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perm_menu
-- ----------------------------
INSERT INTO `perm_menu` VALUES (1, '用户权限模块管理', 'perm', NULL, NULL, 1, NULL);
INSERT INTO `perm_menu` VALUES (2, '菜单管理', 'menu', '/perm/menu/list', 1, 1, NULL);
INSERT INTO `perm_menu` VALUES (3, '菜单权限添加/修改', 'menu_add', '/perm/menu/add', 2, 2, NULL);
INSERT INTO `perm_menu` VALUES (5, '菜单权限删除', 'menu_delete', '/perm/menu/delete', 2, 2, NULL);
INSERT INTO `perm_menu` VALUES (9, 'test', 'test', '/test', 2, 2, NULL);
INSERT INTO `perm_menu` VALUES (10, '用户管理', 'user', '/perm/user/list_ui', 1, 1, NULL);
INSERT INTO `perm_menu` VALUES (11, '用户列表', 'user_list', '/perm/user/list', 10, 2, NULL);
INSERT INTO `perm_menu` VALUES (13, '用户添加', 'user_add', '/perm/user/add', 10, 2, NULL);
INSERT INTO `perm_menu` VALUES (14, '菜单树', 'menu_tree', '/perm/menu/tree', 2, 2, NULL);
INSERT INTO `perm_menu` VALUES (15, '用户禁用', 'user_enable', '/perm/user/enable', 10, 2, NULL);
INSERT INTO `perm_menu` VALUES (16, '角色管理', 'role', '/perm/role/list_ui', 1, 1, NULL);
INSERT INTO `perm_menu` VALUES (17, '角色列表', 'role_list', '/perm/role/list', 16, 2, NULL);
INSERT INTO `perm_menu` VALUES (18, '角色添加', 'role_add', '/perm/role/add', 16, 2, NULL);
INSERT INTO `perm_menu` VALUES (19, '角色修改', 'role_edit', '/perm/role/edit', 16, 2, NULL);
INSERT INTO `perm_menu` VALUES (20, '角色删除', 'role_delete', '/perm/role/delete', 16, 2, NULL);

-- ----------------------------
-- Table structure for perm_role
-- ----------------------------
DROP TABLE IF EXISTS `perm_role`;
CREATE TABLE `perm_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perm_role
-- ----------------------------
INSERT INTO `perm_role` VALUES (1, '管理员', 'admin');
INSERT INTO `perm_role` VALUES (15, 'weir1', 'weir');

-- ----------------------------
-- Table structure for perm_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `perm_role_permission`;
CREATE TABLE `perm_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perm_role_permission
-- ----------------------------
INSERT INTO `perm_role_permission` VALUES (1, 1, 1);
INSERT INTO `perm_role_permission` VALUES (2, 2, 1);
INSERT INTO `perm_role_permission` VALUES (3, 3, 1);
INSERT INTO `perm_role_permission` VALUES (4, 5, 1);
INSERT INTO `perm_role_permission` VALUES (5, 9, 1);
INSERT INTO `perm_role_permission` VALUES (6, 10, 1);
INSERT INTO `perm_role_permission` VALUES (7, 11, 1);
INSERT INTO `perm_role_permission` VALUES (8, 13, 1);
INSERT INTO `perm_role_permission` VALUES (9, 14, 1);
INSERT INTO `perm_role_permission` VALUES (10, 15, 1);
INSERT INTO `perm_role_permission` VALUES (11, 16, 1);
INSERT INTO `perm_role_permission` VALUES (12, 17, 1);
INSERT INTO `perm_role_permission` VALUES (13, 18, 1);
INSERT INTO `perm_role_permission` VALUES (14, 19, 1);
INSERT INTO `perm_role_permission` VALUES (15, 20, 1);
INSERT INTO `perm_role_permission` VALUES (31, 2, 15);
INSERT INTO `perm_role_permission` VALUES (32, 3, 15);
INSERT INTO `perm_role_permission` VALUES (33, 5, 15);
INSERT INTO `perm_role_permission` VALUES (34, 9, 15);
INSERT INTO `perm_role_permission` VALUES (35, 14, 15);
INSERT INTO `perm_role_permission` VALUES (36, 11, 15);
INSERT INTO `perm_role_permission` VALUES (37, 13, 15);
INSERT INTO `perm_role_permission` VALUES (38, 17, 15);
INSERT INTO `perm_role_permission` VALUES (39, 18, 15);
INSERT INTO `perm_role_permission` VALUES (40, 19, 15);

-- ----------------------------
-- Table structure for perm_user
-- ----------------------------
DROP TABLE IF EXISTS `perm_user`;
CREATE TABLE `perm_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pwd` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`, `username`, `pwd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perm_user
-- ----------------------------
INSERT INTO `perm_user` VALUES (1, 'weir', '$2a$10$ucYZcZsz1YOBJqouQqmwR.lRJz7H3HyhXZOlNmlOqEp6aTpON0zP2');
INSERT INTO `perm_user` VALUES (2, 'weir01', '$2a$10$ucYZcZsz1YOBJqouQqmwR.lRJz7H3HyhXZOlNmlOqEp6aTpON0zP2');

-- ----------------------------
-- Table structure for perm_user_role
-- ----------------------------
DROP TABLE IF EXISTS `perm_user_role`;
CREATE TABLE `perm_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perm_user_role
-- ----------------------------
INSERT INTO `perm_user_role` VALUES (1, 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
