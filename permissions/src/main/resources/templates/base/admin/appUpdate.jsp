<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="aurorapt" uri="http://java.aurora.com/aurora/permission"%>
<!DOCTYPE html>
<html>
<head>
<title>APP更新管理</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var appUpdateDataGrid;
$(function() {
	appUpdateDataGrid = $('#appUpdate_datagrid').datagrid({
		url : '${pageContext.request.contextPath}/aotemaiAppUpdate/list',
		fit : true,
		fitColumns : true,
		border : false,
		pagination : true,
		idField : 'id',
		pagePosition : 'both',
		checkOnSelect:true,
		selectOnCheck:true,
		columns : [ [ {
			field : 'id',
			title : '编号',
			width : 1,
			checkbox : true
		}, {
			field : 'appType',
			title : 'app类型',
			width : 50,
			formatter: function(value,row,index){
				if(value == 1){return '安卓';};
				if(value == 2){return 'IOS';};
			}
		}, {
			field : 'appVersion',
			title : 'app版本号',
			width : 50
		}, {
			field : 'name',
			title : '文件名',
			width : 50
		}, {
			field : 'appVersionCode',
			title : '版本递增码',
			width : 30
		}, {
			field : 'appUpdateUrl',
			title : 'app更新地址',
			width : 100
		}, {
			field : 'createTime',
			title : '创建时间',
			width : 80
		}] ],
		onRowContextMenu:function(e, rowIndex, rowData){
			e.preventDefault();
			$(this).datagrid('clearSelections');
			$(this).datagrid('clearChecked');
			$(this).datagrid('selectRow',rowIndex);
			$('#appUpdate_menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
		},toolbar:'#tb'
	});
	
});

function appUpdateAdd() {
	var dialog = parent.modalDialog({
		title : 'APP更新添加',
		width : 700,
		height : 500,
		url : '${pageContext.request.contextPath}/aotemaiAppUpdate/addUI',
		buttons : [ {
			text : '添加',
			handler : function() {
				dialog.find('iframe').get(0).contentWindow.appUpdate_submitForm(dialog, appUpdateDataGrid, parent.$);
			}
		} ]
	});
}

function appUpdateEdit(){
	var rows = appUpdateDataGrid.datagrid('getChecked');
	if(rows.length==1){
		var dialog = parent.modalDialog({
			title : 'APP更新修改',
			width : 700,
			height : 500,
			url : '${pageContext.request.contextPath}/aotemaiAppUpdate/editUI/'+rows[0].id,
			buttons : [ {
				text : '添加',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.appUpdate_submitForm(dialog, appUpdateDataGrid, parent.$);
				}
			} ]
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行修改');
	}
}
function appUpdateDelete(){
	var rows = appUpdateDataGrid.datagrid('getChecked');
	if(rows.length==1){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){
		    if (r){
				$.post('${pageContext.request.contextPath}/aotemaiAppUpdate/delete/'+rows[0].id, function(j) {
					if (j.success) {
						appUpdateDataGrid.datagrid('reload');
					}
					appUpdateDataGrid.datagrid('uncheckAll');
					$.messager.show({
						title : '提示',
						msg : j.msg,
						timeout : 5000,
						showType : 'slide'
					});
				}, 'json').error(function() {
					$.messager.show({
						title : '提示',
						msg : '你的权限不够',
						timeout : 5000,
						showType : 'slide'
					});
				});
		    }    
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行删除');
	}
}


function appUpdate_searchFun() {
	$('#appUpdate_datagrid').datagrid('load', serializeObject($('#appUpdate_searchForm')));
}
function appUpdate_clearFun() {
	$('#appUpdate_searchForm input').val('');
	//$('#admin_yhgl_datagrid').datagrid('load', {});
}
</script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false,title:'用户列表'" style="overflow: hidden;">
		<table id="appUpdate_datagrid"></table>
	</div>
</div>

<div id="tb" style="padding:2px 5px;">
<aurorapt:permission privilege="aotemaiAppUpdate_addUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-add" onclick="appUpdateAdd();">添加</a></aurorapt:permission>
<aurorapt:permission privilege="aotemaiAppUpdate_editUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-pencil" onclick="appUpdateEdit();">修改</a></aurorapt:permission>
<aurorapt:permission privilege="aotemaiAppUpdate_delete">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-pencil" onclick="appUpdateDelete();">删除</a></aurorapt:permission>
<!-- <form id="appUpdate_searchForm">

姓名：<input class="easyui-textbox" name="realname"/>
手机：<input class="easyui-textbox" name="mphone"/>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="appUpdate_searchFun();">查询</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear" onclick="appUpdate_clearFun();">清空</a>
        </form> -->
    </div>

<div id="appUpdate_menu" class="easyui-menu" style="width: 120px;display: none;">
<aurorapt:permission privilege="aotemaiAppUpdate_addUI">
<div onclick="appUpdateAdd()" iconCls="icon-add">增加</div></aurorapt:permission>
<aurorapt:permission privilege="aotemaiAppUpdate_editUI">
<div onclick="appUpdateEdit()" iconCls="icon-edit">编辑</div></aurorapt:permission>
<aurorapt:permission privilege="aotemaiAppUpdate_delete">
<div onclick="appUpdateDelete()" iconCls="icon-edit">删除</div></aurorapt:permission>
</div>
</body>
</html>