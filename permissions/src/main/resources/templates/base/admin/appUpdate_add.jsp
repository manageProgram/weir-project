<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>APP跟新添加</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var appUpdate_submitForm = function(dialog, appUpdate_treeGrid, p) {
	if ($('#appUpdate_addForm').form('validate')) {
		$.post('${pageContext.request.contextPath}/aotemaiAppUpdate/add', $("#appUpdate_addForm").serialize(), function(j) {
			if (j.success) {
				appUpdate_treeGrid.datagrid('reload');
				dialog.dialog('destroy');
			}
			p.messager.show({
				title : '提示',
				msg : j.msg,
				timeout : 5000,
				showType : 'slide'
			});
		}, 'json').error(function() {
			$.messager.show({
				title : '提示',
				msg : '你的权限不够',
				timeout : 5000,
				showType : 'slide'
			});
		});
	}
};
$(function(){
	 var options = {
		        url : "${pageContext.request.contextPath}/aotemaiAppUpdate/upload",
		        dataType : "json",
		        type : "post",
		        success : function(j){
		        	$("#msgshow").html(j.msg);
		        }
		    };
		    $("#pic").change(function(){
		       $("#upload").ajaxSubmit(options)
		    });
});
</script>
</head>

<body>
<form id="upload" method="post" enctype="multipart/form-data">
		<table style="width: 100%;" class="table table-hover table-condensed">
			<tr>
				<th>上传APP包</th>
				<td >
					<input id="pic" type="file" name="pic" style="width:100%;"><span id="msgshow"></span>
				</td>
			</tr>
		</table>
</form>
	<form id="appUpdate_addForm" method="post">
	<input type="hidden" name="id" value="${appUpdate.id}"/>
		<table style="width: 100%;" class="table table-hover table-condensed">
			<tr>
			<th>app类型</th>
				<td>
				<c:if test="${empty appUpdate }">
				  <div class="easyui-radio" id="radio_appType">
							<input type="radio" name="appType" value="1" checked label="安卓">
							<input type="radio" name="appType" value="2" label="IOS">
						</div>
				</c:if>
				<c:if test="${!empty appUpdate }">
				<div class="easyui-radio" id="radio_appType">
							<input type="radio" name="appType" value="1" <c:if test="${appUpdate.appType=='1'}">checked</c:if> label="安卓">
							<input type="radio" name="appType" value="2" <c:if test="${appUpdate.appType=='2'}">checked</c:if> label="IOS">
						</div>
				</c:if>
				</td>
			</tr>
			<tr>
			   <th>文件名</th>
				<td><input name="name" value="${appUpdate.name}" class="easyui-validatebox"
					data-options="required:true,missingMessage:'必填'" /></td>
			</tr>
			<tr>
			   <th>app版本号</th>
				<td><input name="appVersion" value="${appUpdate.appVersion}" class="easyui-validatebox"
					data-options="required:true,missingMessage:'app版本号必填'" /></td>
			</tr>
			<tr>
			   <th>版本递增码</th>
				<td><input name="appVersionCode" value="${appUpdate.appVersionCode}" class="easyui-validatebox"
					data-options="required:true,missingMessage:'app版本号必填'" /></td>
			</tr>
			<tr>
				<th>app更新地址</th>
				<td>
				<c:if test="${empty appUpdate }">
				   <input name="appUpdateUrl" size="80" value="http://aotemai.rsaurora.com.cn/attachment/appUpdate/"/>
				</c:if>
				<c:if test="${!empty appUpdate }">
				  <input name="appUpdateUrl" size="80" value="${appUpdate.appUpdateUrl}"/>
				</c:if>
				</td>
			</tr>
		</table>
	
	</form>
</body>
</html>