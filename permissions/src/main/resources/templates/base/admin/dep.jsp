<%@ page language="java" pageEncoding="UTF-8"%><%@ taglib prefix="aurorapt" uri="http://java.aurora.com/aurora/permission"%>
<!DOCTYPE html>
<html>
<head>
<title>组织架构管理</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var menu_row = -1;
var menu_treeGrid;
	$(function() {
		menu_treeGrid=$('#menu_treeGrid').treegrid({
			url : '${pageContext.request.contextPath}/aotemaiDepartment/listTree',
			idField : 'id',
			treeField : 'name',
			parentField : 'pid',
			fit : true,
			fitColumns : false,
			border : false,
			columns : [ [ {
				title : '编号',
				field : 'id',
				width : 150,
				hidden : true
			}, {
				title : '部门名称',
				field : 'name',
				width : 300
			}, {
				field : 'pid',
				title : '父菜单ID',
				width : 100,
				hidden : true
			}, {
				field : 'pname',
				title : '父菜单',
				width : 300
			} ] ],
			/* toolbar : [ {
				iconCls : 'ext-icon-add',
				text : '添加',
				handler : function() {
					addMenuFun('c');
				}
			} ], */
			onContextMenu : function(e, row) {
				e.preventDefault();
				$(this).datagrid('clearSelections');
				$(this).datagrid('clearChecked');
				$(this).treegrid('select', row.id);
				$('#menu_menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			},toolbar:'#tb'
		});
	});
	
	function addMenuFun(m){
		var pid=null;
		if(menu_treeGrid.treegrid('getSelected')!=null){
			var id = menu_treeGrid.treegrid('getSelected').id;
			if(m=='p'){
				pid=id;
			}else if(m=='c'){
				var node = menu_treeGrid.treegrid('getParent',id);
				if(node!=null){
					pid = menu_treeGrid.treegrid('getParent',id).id;
				}else{
					pid=null;
				}
			}
		}else{
			pid=null;
		}
		var dialog = parent.modalDialog({
			title : '组织架构添加',
			width : 300,
			height : 300,
			url : '${pageContext.request.contextPath}/aotemaiDepartment/addUI',
			buttons : [ {
				text : '添加',
				handler : function() {
				    var menu_add = dialog.find('iframe').get(0).contentWindow;
					menu_add.document.getElementById("menu_pid").value=pid;
					menu_add.menuadd_submitForm(dialog, menu_treeGrid, parent.$);
				}
			} ]
		});
	}
	function deleteMenuFun(){
		var node = menu_treeGrid.treegrid('getSelected');
		if(node){
			parent.$.messager.confirm('确认','您确认想要删除记录吗？',function(r){
				if(r){
					$.post('${pageContext.request.contextPath}/aotemaiDepartment/delete/'+node.id,  function(j) {
						if (j.success) {
							menu_treeGrid.treegrid('reload');
						}
						parent.$.messager.show({
							title : '提示',
							msg : j.msg,
							timeout : 5000,
							showType : 'slide'
						});
					}, 'json').error(function() {
						$.messager.show({
							title : '提示',
							msg : '你的权限不够',
							timeout : 5000,
							showType : 'slide'
						});
					});
				}
			});
		}
	}
	
	function editMenuFun(){
		var node = menu_treeGrid.treegrid('getSelected');
		var dialog = parent.modalDialog({
			title : '组织架构修改',
			width : 300,
			height : 300,
			url : '${pageContext.request.contextPath}/aotemaiDepartment/editUI/'+node.id,
			buttons : [ {
				text : '添加',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.menuadd_submitForm(dialog, menu_treeGrid, parent.$);
				}
			} ]
		});
	}
</script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;">
		<table id="menu_treeGrid"></table>
	</div>
</div>

<div id="tb" style="padding:2px 5px;">
<aurorapt:permission privilege="department_addUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-add" onclick="addMenuFun('c');">添加</a></aurorapt:permission>
</div>

<div id="menu_menu" class="easyui-menu" style="width: 120px; display: none;">
<aurorapt:permission privilege="department_addUI">
	<div onclick="addMenuFun('p');" data-options="iconCls:'icon-mini-add'">增加子节点</div>
	<div onclick="addMenuFun('c');" data-options="iconCls:'icon-mini-add'">增加同级节点</div>
	<div onclick="editMenuFun();" data-options="iconCls:'icon-mini-edit'">编辑</div></aurorapt:permission>
	<aurorapt:permission privilege="department_delete">
	<div onclick="deleteMenuFun();" data-options="iconCls:'icon-cancel'">删除</div></aurorapt:permission>
</div>
</body>
</html>