<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>组织架构添加与修改</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var menuadd_submitForm = function(dialog, menu_treeGrid, p) {
	if ($('#menu_addForm').form('validate')) {
		$.post('${pageContext.request.contextPath}/aotemaiDepartment/add', $("#menu_addForm").serialize(), function(j) {
			if (j.success) {
				menu_treeGrid.treegrid('reload');
				dialog.dialog('destroy');
			}
			p.messager.show({
				title : '提示',
				msg : j.msg,
				timeout : 5000,
				showType : 'slide'
			});
		}, 'json').error(function() {
			$.messager.show({
				title : '提示',
				msg : '你的权限不够',
				timeout : 5000,
				showType : 'slide'
			});
		});
	}
};
</script>
</head>

<body>
	<form id="menu_addForm" method="post">
		<input type="hidden" id="menu_pid" name="pepartmentId" value="${department.pepartmentId}"/>
		<input type="hidden" id="menu_id" name="id" value="${department.id}"/>
		<table class="table table-hover table-condensed">
			<tr>
				<th>组织架构名称</th>
				<td><input type="text" name="name" value="${department.name}" class="easyui-validatebox" data-options="required:true,missingMessage:'组织架构名称必填'" /></td>
			</tr>
		</table>
	</form>
</body>
</html>