<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>系统操作日志管理</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var logDataGrid;
$(function() {
	logDataGrid = $('#log_datagrid').datagrid({
		url : '${pageContext.request.contextPath}/aotemaiSystemOperationLog/list',
		fit : true,
		fitColumns : true,
		border : false,
		pagination : true,
		idField : 'id',
		pagePosition : 'both',
		checkOnSelect:true,
		selectOnCheck:true,
		columns : [ [
			{field : 'id',	title : '编号',	width : 100,hidden : true}, 
			{field : 'ip',title : 'IP',width : 100},
			{field : 'userName',title : '操作者',width : 50}, 
			{field : 'createTime',title : '操作时间',width : 120},
			{field : 'describle',	title : '操作描述',	width : 100}, 
			{field : 'typeName',title : '类型',width : 30	}, 
			{field : 'method',	title : '请求方法',	width : 300},
			{field : 'params',title : '请求参数',	width : 100	},
			{field : 'exceptionCode',title : '异常代码',width : 100}, 
			{field : 'exceptionDetail',title : '异常信息',width : 100}

		] ],
		onLoadSuccess:function(){
            $(this).datagrid('doCellTip',{'max-width':'300px','delay':500});
        },
		onRowContextMenu:function(e, rowIndex, rowData){
			e.preventDefault();
			$(this).datagrid('clearSelections');
			$(this).datagrid('clearChecked');
			$(this).datagrid('selectRow',rowIndex);
			$('#log_menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
		},toolbar:'#tb'
	});
	
});
function logView(){
	var rows = logDataGrid.datagrid('getChecked');
	if(rows.length==1){
		var dialog = parent.modalDialog({
			title : '查看详情',
			width : 700,
			height : 400,
			url : '${pageContext.request.contextPath}/aotemaiSystemOperationLog/view/'+rows[0].id
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行查看');
	}
}

function log_searchFun() {
	$('#log_datagrid').datagrid('load', serializeObject($('#log_searchForm')));
}
function log_clearFun() {
	$('#log_searchForm input').val('');
	$('#log_searchForm input').attr('checked',false);
}
</script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false,title:'日志列表'" style="overflow: hidden;">
		<table id="log_datagrid"></table>
	</div>
</div>


<div id="tb" style="padding:2px 5px;">
	<form id="log_searchForm">
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-tip" onclick="logView();">查看</a>
		类型：<input type="radio" name="type" value="1">请求<input type="radio" name="type" value="2">异常
		姓名：<input class="easyui-textbox" name="userName"/>
		操作描述：<input class="easyui-textbox" name="dascribe"/>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="log_searchFun();">查询</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear" onclick="log_clearFun();">清空</a>
    </form>
</div>

<div id="log_menu" class="easyui-menu" style="width: 120px;display: none;">
<div onclick="logView()" iconCls="icon-tip">查看详情</div>
</div>
</body>
</html>