<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>日志明细</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
</script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false,title:'日志明细'">
		<table class="table table-hover table-condensed">
			<tr>
				<th>用户</th>
				<td>${log.userName}</td>
				<th>记录时间</th>
				<td>${log.createTimeStr}</td>
			</tr>
			<tr>
				<th>事件描述</th>
				<td>${log.describle}</td>
				<th>类型</th>
				
				<td>
					<c:if test="${log.type == 1}">请求</c:if>
					<c:if test="${log.type == 2}">异常</c:if>
				</td>
			</tr>
			<tr>
				<th>请求方法</th>
				<td>${log.method}</td>
				<th>IP</th>
				<td>${log.ip}</td>
			</tr>
			<tr>
				<th>请求参数</th>
				<td colspan="3">${log.params}</td>
			</tr>
			<tr>
				<th>异常代码</th>
				<td colspan="3">${log.exceptionCode}</td>
			</tr>
			<tr>
				<th>异常信息</th>
				<td colspan="3">${log.exceptionDetail}</td>
			</tr>
		</table>
	</div>
</div>

</body>
</html>