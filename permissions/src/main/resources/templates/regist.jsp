<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="奥特卖" content="yes">
<meta name="奥特卖" content="black">
<title>奥特卖</title>
<link href="${pageContext.request.contextPath}/js/h5_regist/css/style_new.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="min_top">注册</div>
<div class="login_top_img"><img src="${pageContext.request.contextPath}/js/h5_regist/images/app_down_top.jpg" class="top_bg"  width="640" height="300" />
  <div class="app_down_brn">
    <ul>
      <li><a href="http://aotemai.rsaurora.com.cn/front/account/file2Stream"><img src="${pageContext.request.contextPath}/js/h5_regist/images/down_android.png" width="139" height="49" alt=""/></a></li>
      <li><a href="https://itunes.apple.com/us/app/aotemai/id1289233694?l=zh&ls=1&mt=8"><img src="${pageContext.request.contextPath}/js/h5_regist/images/down_ios.png" width="139" height="49" alt=""/></a></li>
    </ul>
  </div>
</div>
<form id="form_regist" method="post" action="${pageContext.request.contextPath}/front/member/registh">
<input type="hidden" name="memberId" value="${memberId }"/>
<div class="white_bg register_box">
  <ul>
     <li><span>手机号码:</span>
      <input type="text" class="input_login" id="mobile" name="mobile" placeholder="请输入手机号码">
    </li>
    <li><span>验&nbsp;证&nbsp;码&nbsp;:</span>
      <input type="text" class="input_login_small" id="code" name="code" placeholder="请输入验证码">
      <a href="javascript:void(0);" id="sendSms" onclick="sendPhoneSms();">发送验证码</a>
    </li>
    <li><span>登录密码:</span>
      <input type="password" class="input_login" id="pwd" name="pwd" placeholder="6-12位数字字母组合">
    </li>
    <li><span>确认密码:</span>
      <input type="password" class="input_login" id="pwd2" name="pwd2" placeholder="请输再次输入密码">
    </li>
    <!-- <li><span>微信号码:</span>
      <input type="text" class="input_login" placeholder="请输入微信号码">
    </li> -->
  </ul>
</div>
<div class="div_btn" >
<input type="submit" id="inputSubmit" class="btn_brown_round" value="确认提交">
<!-- <a href="#" class="btn_brown_round">确认提交</a>  --></div>
</form>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="${pageContext.request.contextPath}/js/login/assets/js/html5shiv.js"></script>
		<script src="${pageContext.request.contextPath}/js/login/assets/js/respond.js"></script>
		<![endif]-->
<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/js/login/assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/js/login/assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='${pageContext.request.contextPath}/js/login/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
<script type="text/javascript" charset="UTF-8" src="${pageContext.request.contextPath}/js/jquery.form.min.js"></script>
<script type="text/javascript">
function sendPhoneSms(){
	if($('#mobile').val().length == 0){
		$('#mobile').focus();
		alert("请输入手机号");
		return false;
	}
	$.post('${pageContext.request.contextPath}/front/member/sendSmsCode/'+$('#mobile').val(), function(j) {
		if(j.success){
			showtime(60);
		}else{
			alert(j.msg);
		}
	}, 'json').error(function() {
		alert("你的权限不够");
	});
}
function showtime(t){
	$("#sendSms").attr("disabled",true);
	for(i=1;i<=t;i++) {
		window.setTimeout("update_p(" + i + ","+t+")", i * 1000);
	}
}

function update_p(num,t) {
	if(num == t) {
		$("#sendSms").html("重新发送");
		$("#sendSms").attr("disabled",false);
	}else {
		printnr = t-num;
		$("#sendSms").html(' (' + printnr +')秒后再发');
	}
}

$('#form_regist').ajaxForm({
    beforeSubmit: function(a,f,o) {
		if($('#mobile').val().length == 0){
			$('#mobile').focus();
			alert("请输入手机号");
			return false;
		}
		if($('#code').val().length == 0){
			$('#code').focus();
			alert("请输入手机验证码");
			return false;
		}
		if($('#pwd').val().length == 0 && $('#pwd2').val().length == 0){
			$('#pwd').focus();
			alert("请输入登录密码或确认密码");
			return false;
		}
		if($('#pwd').val() != $('#pwd2').val()){
			alert("两次输入的密码不一致");
		}
		$("#inputSubmit").val("正在提交中...");
		$("#inputSubmit").attr("disabled","disabled");
    },
    success: function(json) {
    	if(json.success){
    		$("#inputSubmit").val("请下载APP登陆");
    		//location.replace('${pageContext.request.contextPath}/main');
    	}else{
    		alert(json.msg);
    		$("#inputSubmit").val("重新提交");
    		$("#inputSubmit").attr("disabled",false);
    	}
    }
});
</script>
</html>
