package com.weir.resources.permissions;
import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.Event;

public class BinlogParse {

    public static void main(String[] args) throws Exception {
        final BinaryLogClient client = new BinaryLogClient("127.0.0.1", 3306, "root", "336393");
        client.setBinlogFilename("mysql-bin.001084");
        client.setBinlogPosition(123);
        client.registerEventListener(new BinaryLogClient.EventListener() {

            public void onEvent(Event event) {

                System.out.println(event.toString());
                System.out.println(client.getBinlogPosition());

            }
        });
        client.connect();
    }
}
