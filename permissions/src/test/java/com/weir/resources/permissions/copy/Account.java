package com.weir.resources.permissions.copy;

public class Account {

	private Integer userId;
	private Double money;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	@Override
	public String toString() {
		return "Account [userId=" + userId + ", money=" + money + "]";
	}
}
