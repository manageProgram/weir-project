package com.weir.resources.permissions.copy;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * @description:
 * @author: yangzhou
 * @date: 2020-01-15 09:12:00
 * @since: 1.0.0
 **/
public class ForwardsOrgDTO {
    @NotNull
    private Long orgId;
    private List<Users> users;

    public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}
    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

	@Override
	public String toString() {
		return "ForwardsOrgDTO [orgId=" + orgId + ", users=" + users + "]";
	}
}
