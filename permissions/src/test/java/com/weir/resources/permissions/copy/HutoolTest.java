package com.weir.resources.permissions.copy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import cn.hutool.core.util.ObjectUtil;

public class HutoolTest {

	public static void main(String[] args) {

		ForwardsOrgVO forwardsOrgVO = new ForwardsOrgVO();
		forwardsOrgVO.setOrgId(222222222222L);
		List<Users> users = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Users u = new Users();
			u.setName("weir" + i);
			u.setPwd("weir" + i);
			List<Account> accounts = new ArrayList<>();
			for (int j = 0; j < 2; j++) {
				Account account = new Account();
				account.setUserId(j);
				account.setMoney(Double.valueOf(j));
				accounts.add(account);
			}
			u.setAccounts(accounts);
			users.add(u);
		}
		forwardsOrgVO.setUsers(users);
		ForwardsOrgDTO forwardsOrgDTO = new ForwardsOrgDTO();
		BeanUtils.copyProperties(forwardsOrgVO, forwardsOrgDTO);
		System.out.println(forwardsOrgDTO.toString());
		System.out.println("--------------------------------------");
//		ForwardsOrgVO cloneByStream = ObjectUtil.cloneByStream(forwardsOrgVO);
//		System.out.println(cloneByStream.toString());
		System.out.println("--------------------------------------");
		ForwardsOrgDTO forwardsOrgDTO2 = BeanUtilsExt.deepCopy(forwardsOrgVO, ForwardsOrgDTO.class);
		System.out.println(forwardsOrgDTO2.toString());
	}

}
