package com.weir.resources.permissions.copy;

public interface ICloneable extends Cloneable {

	Object clone();

}
