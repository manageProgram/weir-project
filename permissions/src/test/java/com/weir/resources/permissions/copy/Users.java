package com.weir.resources.permissions.copy;

import java.util.List;

public class Users {

	private String name;
	private String pwd;
	
	private List<Account> accounts;
	
	public List<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	@Override
	public String toString() {
		return "Users [name=" + name + ", pwd=" + pwd + ", accounts=" + accounts + "]";
	}
}
