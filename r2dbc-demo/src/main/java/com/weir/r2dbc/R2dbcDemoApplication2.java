//package com.weir.r2dbc;
//
//import org.reactivestreams.Publisher;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
//import org.springframework.data.r2dbc.repository.Query;
//import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
//import org.springframework.data.repository.reactive.ReactiveCrudRepository;
//import org.springframework.data.repository.reactive.ReactiveSortingRepository;
//import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import dev.miku.r2dbc.mysql.MySqlConnectionConfiguration;
//import dev.miku.r2dbc.mysql.MySqlConnectionFactory;
//import io.r2dbc.spi.ConnectionFactories;
//import io.r2dbc.spi.ConnectionFactory;
//import io.swagger.v3.oas.annotations.responses.ApiResponse;
//import io.swagger.v3.oas.annotations.responses.ApiResponses;
//import io.swagger.v3.oas.models.Components;
//import io.swagger.v3.oas.models.OpenAPI;
//import io.swagger.v3.oas.models.info.Info;
//import io.swagger.v3.oas.models.info.License;
//import io.swagger.v3.oas.models.security.SecurityScheme;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//
//@SpringBootApplication
//public class R2dbcDemoApplication2 {
//
//	public static void main(String[] args) {
//		SpringApplication.run(R2dbcDemoApplication2.class, args);
//	}
//
//	@Bean
//	public OpenAPI customOpenAPI(@Value("${springdoc.version}") String appVersion) {
//		return new OpenAPI()
//				.components(new Components()
//						.addSecuritySchemes("basicScheme",new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic")))
//				.info(new Info().title("Tweet API").version(appVersion)
//						.license(new License().name("Apache 2.0").url("http://springdoc.org")));
//	}
//}
//
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//class Orders {
//
//	@Id
//	private Long id;
//
//	private String fn;
//
//}
//@Repository
//interface OrdersRepository extends ReactiveSortingRepository<Orders, Long> {
//	@Query("SELECT * FROM orders LIMIT :limit OFFSET :offset")
//	Flux<Orders> findById(int offset, int limit);
//
//}
//
//@Service
//class OrderService {
//	private final OrdersRepository ordersRepository;
//
//	public OrderService(OrdersRepository ordersRepository) {
//		super();
//		this.ordersRepository = ordersRepository;
//	}
//
//	public Flux<Orders> findAll() {
//		return ordersRepository.findAll(Sort.unsorted());
//	}
//}
//
//@RestController
//@RequestMapping("/order")
//class OrdersController {
//	@Autowired
//	OrderService orderService;
//
//	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "order_all") })
//	@GetMapping("/order_all")
//	public Flux<Orders> findAll() {
//		return orderService.findAll();
//	}
//
////	@GetMapping("/init")
////	public void init() {
////		for (int i = 0; i < 20; i++) {
////			Orders orders = new Orders();
////			orders.setId(10l + i);
////			orders.setFn("weir" + i);
////			ordersRepository.save(orders);
////		}
////	}
//
////	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "page") })
////	@GetMapping("/page")
////	public Flux<Orders> page() {
////		return ordersRepository.findById(0, 10);
////	}
//}
//
//@Configuration
//@EnableR2dbcRepositories
//class ApplicationConfig extends AbstractR2dbcConfiguration {
//
//  @Bean
//  public ConnectionFactory connectionFactory() {
//    return ConnectionFactories.get("r2dbc:mysql://root:336393@localhost:3306/orders");
//  }
//}
