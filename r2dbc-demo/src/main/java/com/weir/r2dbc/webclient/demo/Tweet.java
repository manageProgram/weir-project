package com.weir.r2dbc.webclient.demo;
 
public class Tweet {
 
	private String desc;
	
	private String user;
 
	public Tweet() {
		
	}
	
	public Tweet(String desc, String user) {
		super();
		this.desc = desc;
		this.user = user;
	}
 
 
 
	public String getDesc() {
		return desc;
	}
 
	public void setDesc(String desc) {
		this.desc = desc;
	}
 
	public String getUser() {
		return user;
	}
 
	public void setUser(String user) {
		this.user = user;
	}
	
	
}
