package com.weir.rsocket.consumer;

import java.net.InetSocketAddress;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.rsocket.MetadataExtractor;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.util.MimeTypeUtils;

import io.rsocket.RSocket;
import io.rsocket.RSocketFactory;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.transport.netty.client.TcpClientTransport;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@Configuration
public class ConsumerConfiguration {

//	@Bean
//	RSocket rSocket() {
//		return RSocketFactory.connect().dataMimeType(MimeTypeUtils.APPLICATION_JSON_VALUE)
//				.frameDecoder(PayloadDecoder.ZERO_COPY).transport(TcpClientTransport.create("127.0.0.1", 9998)).start()
//				.block();
//	}
//
//	@Bean
//	RSocketRequester requester(RSocketStrategies rSocketStrategies) {
//		log.info("ConsumerConfiguration------");
//		return RSocketRequester.wrap(this.rSocket(), MimeTypeUtils.APPLICATION_JSON, MimeTypeUtils.APPLICATION_JSON,
//				rSocketStrategies);
//	}
//	@Bean
//    RSocketRequester rSocketRequester(RSocketStrategies rSocketStrategies) {
//        return RSocketRequester.builder()
//                .rsocketStrategies(rSocketStrategies)
//                .connectTcp("localhost", 9998)
//                .block();
//    }
	
	@Bean
	RSocket rSocket() {
	    return RSocketFactory.connect()
	            .mimeType(MetadataExtractor.ROUTE_KEY.toString(), MimeTypeUtils.APPLICATION_JSON_VALUE)
	            .frameDecoder(PayloadDecoder.ZERO_COPY)
	            .transport(TcpClientTransport.create(new InetSocketAddress(9998)))
	            .start()
	            .block();
	}

	@Bean
	RSocketRequester requester(RSocketStrategies strategies) {
	    return RSocketRequester.wrap(rSocket(), MimeTypeUtils.APPLICATION_JSON, MimeTypeUtils.APPLICATION_JSON, strategies);
	}
}
