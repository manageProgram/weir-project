package com.weir.rsocket.consumer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
class Orders {

	private Long id;

	private String fn;

	public Orders(Long id) {
		super();
		this.id = id;
	}

}