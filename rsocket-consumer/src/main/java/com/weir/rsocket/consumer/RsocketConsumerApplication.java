package com.weir.rsocket.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsocketConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsocketConsumerApplication.class, args);
	}

}
