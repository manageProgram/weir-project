package com.weir.rsocket.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class RsocketProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsocketProducerApplication.class, args);
	}

}
@Data
@AllArgsConstructor
@NoArgsConstructor
class Orders {

	@Id
	private Long id;

	private String fn;

}

interface OrdersRepository extends ReactiveCrudRepository<Orders, Long> {
}

@RestController
class OrdersController{
	@Autowired
	OrdersRepository ordersRepository;
	
	@GetMapping("/order_all")
	public Flux<Orders> findAll() {
		return ordersRepository.findAll();
	}
}
@Slf4j
@Controller
class MarketDataRSocketController {
    @Autowired
    OrdersRepository ordersRepository;

    @MessageMapping("orderAll")
    public Flux<Orders> findAll() {
    	log.info("MarketDataRSocketController---------------");
        return ordersRepository.findAll();
    }
    @MessageMapping("findById")
    public Mono<Orders> findById(Orders o) {
    	log.info("MarketDataRSocketController-------findById--------");
    	return ordersRepository.findById(o.getId());
    }
//
//    @MessageMapping("feedMarketData")
//    public Flux<MarketData> feedMarketData(MarketDataRequest marketDataRequest) {
//        return marketDataRepository.getAll(marketDataRequest.getStock());
//    }
//
//    @MessageMapping("collectMarketData")
//    public Mono<Void> collectMarketData(MarketData marketData) {
//        marketDataRepository.add(marketData);
//        return Mono.empty();
//    }
//
//    @MessageExceptionHandler
//    public Mono<MarketData> handleException(Exception e) {
//        return Mono.just(MarketData.fromException(e));
//    }
}
