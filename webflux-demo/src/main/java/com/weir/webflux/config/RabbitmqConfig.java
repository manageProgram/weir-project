package com.weir.webflux.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitmqConfig {

    public static final String COLLISION_BACK_EXCHANGE = "weir_collision";

    public static final String COLLISION_BACK_QUEUE = "weir_collision";

    public static final String COLLISION_BACK_ROUTING_KEY = "weir.collision.xyt.back";

    /**
     * @return Queue
     */
    @Bean
    public Queue collisionBackQueue() {
        return new Queue(COLLISION_BACK_QUEUE);
    }

    /**
     * tcp server exchange
     *
     * @return DirectExchange
     */
    @Bean
    public TopicExchange collisionBackExchange() {
        return new TopicExchange(COLLISION_BACK_EXCHANGE);
    }

    /**
     * @param queue    队列
     * @param exchange TopicExchange
     * @return Binding Binding
     */
    @Bean
    Binding collisionBackBinding(@Qualifier("collisionBackQueue") Queue queue,
                            @Qualifier("collisionBackExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(COLLISION_BACK_ROUTING_KEY);
    }
}
