package com.weir.webflux.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.weir.webflux.entity.AotemaiUsers;
import com.weir.webflux.mongo.entity.ArrivedStatusInfo;
import com.weir.webflux.mongo.repository.ArrivedStatusInfoRepository;
import com.weir.webflux.service.IAotemaiUsersService;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author weir
 * @since 2019-12-12
 */
@RestController
@RequestMapping("/user")
public class AotemaiUsersController {

	@Autowired
	private IAotemaiUsersService aotemaiUsersService;
	@Autowired
	private ArrivedStatusInfoRepository arrivedStatusInfoRepository;
	
	@GetMapping("/findByVehicleId/{vehicleId}")
	public List<ArrivedStatusInfo> findByVehicleId(@PathVariable Integer vehicleId) {
		return arrivedStatusInfoRepository.findByVehicleId(vehicleId);
	}
	
	@GetMapping("/list_all")
	public List<AotemaiUsers> listAll() {
		return aotemaiUsersService.list();
	}
	
	@GetMapping("/get/{id}")
	public AotemaiUsers get(@PathVariable Integer id) {
		return aotemaiUsersService.get(id);
	}
}

