package com.weir.webflux.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author weir
 * @since 2019-12-12
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
public class AotemaiUsers extends Model<AotemaiUsers> {

    private static final long serialVersionUID=1L;

      /**
     * ID
     */
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 编号
     */
      private String no;

      /**
     * 用户名
     */
      private String name;

      /**
     * 密码
     */
      private String pwd;

      /**
     * 商家ID
     */
      private Integer shopId;

      /**
     * 邮箱
     */
      private String email;

      /**
     * 手机号
     */
      private String mphone;

      /**
     * 真实姓名
     */
      private String realname;

      /**
     * 部门ID
     */
      private Integer depid;

      /**
     * 性别
     */
      private String sex;

      /**
     * 生日
     */
      private LocalDate birthday;

      /**
     * 入职时间
     */
      private LocalDate entrytime;

      /**
     * 地区
     */
      private String region;

      /**
     * 微信
     */
      private String weixin;

      /**
     * 转正时间
     */
      private LocalDate promotiontime;

      /**
     * 离职时间
     */
      private LocalDate quittime;

      /**
     * 头像(取附件Id)
     */
      private String head;

      /**
     * 是否注销（0否1是）
     */
      private Integer isCancel;


    @Override
    protected Serializable pkVal() {
          return this.id;
      }

}
