package com.weir.webflux.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.weir.webflux.entity.AotemaiUsers;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2019-12-12
 */
public interface AotemaiUsersMapper extends BaseMapper<AotemaiUsers> {

}
