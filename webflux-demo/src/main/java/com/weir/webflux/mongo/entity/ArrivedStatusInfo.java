package com.weir.webflux.mongo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * desc:
 * author:yangzhou
 * date:2019/4/3 14:19
 */
@JsonIgnoreProperties
@Document(collection = "vus_arrived_status")
@CompoundIndexes({
        // 唯一复合索引,
        @CompoundIndex(name = "rule_id_1_arrived_time_1", def = "{'rule_id' : 1, 'arrived_time': 1}",
                unique = true)
})
@Data
public class ArrivedStatusInfo {
    @Field("rule_id")
    @JsonProperty("ruleId")
    private Integer ruleId;

    @Field("arrived_remind_id")
    @JsonProperty("arrivedRemindId")
    private Integer arrivedRemindId;

    @Field("create_ts")
    @JsonProperty("createTs")
    private Long createTs;

    @Field("arrived_staus")
    @JsonProperty("arrivedStatus")
    private String arrivedStatus;

    @Field("arrived_time")
    @JsonProperty("arrivedTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date arrivedtime;

    @Field("vehicle_id")
    @JsonProperty("vehicleId")
    private Integer vehicleId;

    @Field("remind_user")
    @JsonProperty("remindUser")
    private String remindUser;

    @Field("use_show")
    @JsonProperty("useShow")
    private boolean useShow = true;

    @Field("write_mysql")
    @JsonProperty("writeMysql")
    private boolean writeMysql = false;

    @Field("operate_time")
    @JsonProperty("operateTime")
    private Date operateTime = new Date();

}
