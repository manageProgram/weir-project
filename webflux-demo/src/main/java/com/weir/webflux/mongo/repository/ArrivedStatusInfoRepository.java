package com.weir.webflux.mongo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.weir.webflux.mongo.entity.ArrivedStatusInfo;

public interface ArrivedStatusInfoRepository extends MongoRepository<ArrivedStatusInfo, String> {

	List<ArrivedStatusInfo> findByVehicleId(Integer vehicleId);
	
}
