package com.weir.webflux.mq.listener;

import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.amqp.support.AmqpHeaders;

@Slf4j
@Component
public class DemoMqListener {

	@RabbitListener(

            bindings = @QueueBinding(value = @Queue(value = "range_reminder_auto", durable = "true"),
                    exchange = @Exchange(value = "tcp_server_range",
                            type = ExchangeTypes.HEADERS, durable = "true", ignoreDeclarationExceptions = "true"),
                    key = "range_reminder_auto"))
    public void rangeReminderNoticeMsg(
            @Payload Message payload, @Header(AmqpHeaders.CHANNEL) Channel channel,
//            @Payload RangeMqMessage<MonitorMessage> rangeMqMessage, @Header(AmqpHeaders.CHANNEL) Channel channel,
            @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {
		String data = new String(payload.getBody());
		log.info("rangeReminderNoticeMsg-------{}", data);
	}
}
