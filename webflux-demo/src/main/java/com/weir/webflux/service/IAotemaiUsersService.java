package com.weir.webflux.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.weir.webflux.entity.AotemaiUsers;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author weir
 * @since 2019-12-12
 */
public interface IAotemaiUsersService extends IService<AotemaiUsers> {

	AotemaiUsers get(Integer id);

}
