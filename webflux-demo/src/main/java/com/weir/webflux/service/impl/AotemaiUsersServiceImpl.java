package com.weir.webflux.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.weir.webflux.entity.AotemaiUsers;
import com.weir.webflux.mapper.AotemaiUsersMapper;
import com.weir.webflux.service.IAotemaiUsersService;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author weir
 * @since 2019-12-12
 */
@Service
public class AotemaiUsersServiceImpl extends ServiceImpl<AotemaiUsersMapper, AotemaiUsers> implements IAotemaiUsersService {

	@Override
	@Cacheable(value = "weir_user")
	public AotemaiUsers get(Integer id) {
		return getById(id);
	}
}
