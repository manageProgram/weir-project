package com.weir.commons.utils;

import com.weir.commons.vo.Json;
/**
 * 
 * @author weir
 *
 * 2019年3月14日 下午9:52:02
 */
public class JsonUtil {

	/**
	 * 成功返回
	 * @param msg
	 * @return
	 */
	public static Json jsonSuccess(String msg){
		Json json = new Json();
		json.setMsg(msg);
		json.setSuccess(true);
		return json;
	}
	/**
	 * 成功返回
	 * @param msg
	 * @return
	 */
	public static Json jsonSuccess(String msg,Object obj){
		Json json = new Json();
		json.setMsg(msg);
		json.setSuccess(true);
		json.setObj(obj);
		return json;
	}
	/**
	 * 失败返回
	 * @param msg
	 * @return
	 */
	public static Json jsonError(String msg){
		Json json = new Json();
		json.setMsg(msg);
		json.setSuccess(false);
		return json;
	}
	public static Json jsonError(String msg,Object obj){
		Json json = new Json();
		json.setMsg(msg);
		json.setSuccess(false);
		json.setObj(obj);
		return json;
	}
	/**
	 * 自定义返回
	 * @param success
	 * @param msg
	 * @param obj
	 * @return
	 */
	public static Json json(boolean success,String msg,Object obj){
		Json json = new Json();
		json.setMsg(msg);
		json.setSuccess(success);
		json.setObj(obj);
		return json;
	}
}
