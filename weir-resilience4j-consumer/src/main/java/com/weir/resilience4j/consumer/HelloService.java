package com.weir.resilience4j.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.annotation.Retry;

@Service
@Retry(name = "retryBackendA")
public class HelloService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CircuitBreakerRegistry circuitBreakerRegistry;

    public String hello(String name) {
        return restTemplate.getForObject("http://localhost:9999/hell?name={1}", String.class,name);
    }

}
