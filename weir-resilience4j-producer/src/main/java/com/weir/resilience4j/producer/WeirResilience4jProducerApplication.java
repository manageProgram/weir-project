package com.weir.resilience4j.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class WeirResilience4jProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeirResilience4jProducerApplication.class, args);
	}

}
@RestController
class HelloController{
	
	@GetMapping("/hell")
	public String hell(String name) {
		return name + "world";
	}
}