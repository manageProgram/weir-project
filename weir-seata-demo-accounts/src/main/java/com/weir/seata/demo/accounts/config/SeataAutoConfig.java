package com.weir.seata.demo.accounts.config;
 
import javax.sql.DataSource; 
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariDataSource;

import io.seata.rm.datasource.DataSourceProxy;
import io.seata.spring.annotation.GlobalTransactionScanner;
 
@Configuration
public class SeataAutoConfig {
	private final static Logger logger = LoggerFactory.getLogger(SeataAutoConfig.class);
 
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	@Bean
	public DataSource hikariDataSource() {
		HikariDataSource hikariDataSource = new HikariDataSource();
		logger.info("装载dataSource........");
		return hikariDataSource;
	}
 
	/**
	 * init global transaction scanner
	 *
	 * @Return: GlobalTransactionScanner
	 */
//	@Bean
//	public GlobalTransactionScanner globalTransactionScanner() {
//		logger.info("配置seata........");
//		return new GlobalTransactionScanner("weir-seata-demo-order", "default");
//	}
	
//	@Primary
//	@Bean("dataSourceProxy")
//	public DataSourceProxy dataSourceProxy(DataSource dataSource) {
//		return new DataSourceProxy(dataSource);
//	}

	@Bean("jdbcTemplate")
//	@ConditionalOnBean(DataSourceProxy.class)
	public JdbcTemplate jdbcTemplate(DataSource hikariDataSource) {
		return new JdbcTemplate(hikariDataSource);
	}

}
