package com.weir.seata.demo.accounts.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.weir.seata.demo.accounts.entity.Accounts;
import com.weir.seata.demo.accounts.service.IAccountsService;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@RestController
@RequestMapping("/accounts/accounts")
@Slf4j
public class AccountsController {

	@Autowired
	private IAccountsService accountsService;
	
	@GetMapping("/save/{userId}")
	public void save(@PathVariable Integer userId,@RequestParam Double money) {
		log.info("-----扣款开始---");
		Accounts accounts = accountsService.getOne(new QueryWrapper<Accounts>().eq("user_id", userId));
		if (accounts == null) {
			return;
		}
		accounts.setMoney(accounts.getMoney() - money);
		boolean updateById = accountsService.updateById(accounts);
		log.info("-----扣款完成---" + updateById);
	}
}

