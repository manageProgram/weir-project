package com.weir.seata.demo.accounts.mapper;

import com.weir.seata.demo.accounts.entity.Accounts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface AccountsMapper extends BaseMapper<Accounts> {

}
