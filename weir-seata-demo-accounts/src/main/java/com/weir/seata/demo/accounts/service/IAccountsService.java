package com.weir.seata.demo.accounts.service;

import com.weir.seata.demo.accounts.entity.Accounts;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface IAccountsService extends IService<Accounts> {

}
