package com.weir.seata.demo.accounts.service.impl;

import com.weir.seata.demo.accounts.entity.Accounts;
import com.weir.seata.demo.accounts.mapper.AccountsMapper;
import com.weir.seata.demo.accounts.service.IAccountsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@Service
public class AccountsServiceImpl extends ServiceImpl<AccountsMapper, Accounts> implements IAccountsService {

}
