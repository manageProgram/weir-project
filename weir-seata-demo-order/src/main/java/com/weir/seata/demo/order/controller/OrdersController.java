package com.weir.seata.demo.order.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.weir.seata.demo.order.entity.Orders;
import com.weir.seata.demo.order.service.IOrdersService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@RestController
@RequestMapping("/order/orders")
public class OrdersController {

	@Autowired
	private IOrdersService ordersService;
	
	@GetMapping("/list")
	public List<Orders> list() {
		return ordersService.list();
	}
	
	@PostMapping("/save_seata")
	public String saveSeata(@RequestBody Orders orders) {
		ordersService.saveAll(orders);
		return "ok";
	}
}

