package com.weir.seata.demo.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "weir-seata-demo-accounts")
public interface AccountApi {

	@GetMapping("/accounts/accounts/save/{userId}")
	void save(@PathVariable Integer userId,@RequestParam Double money);
}
