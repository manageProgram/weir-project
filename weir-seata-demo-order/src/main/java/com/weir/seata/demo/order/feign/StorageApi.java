package com.weir.seata.demo.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "weir-seata-demo-storages")
public interface StorageApi {

	@GetMapping("/storages/storages/save/{storageCode}/{storageCount}")
	void save(@PathVariable String storageCode,@PathVariable Integer storageCount);
}
