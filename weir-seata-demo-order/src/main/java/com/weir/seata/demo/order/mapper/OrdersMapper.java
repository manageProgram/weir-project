package com.weir.seata.demo.order.mapper;

import com.weir.seata.demo.order.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
