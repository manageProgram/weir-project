package com.weir.seata.demo.order.service;

import com.weir.seata.demo.order.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface IOrdersService extends IService<Orders> {

	void saveAll(Orders orders);

}
