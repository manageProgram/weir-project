package com.weir.seata.demo.order.service.impl;

import com.weir.seata.demo.order.entity.Orders;
import com.weir.seata.demo.order.feign.AccountApi;
import com.weir.seata.demo.order.feign.StorageApi;
import com.weir.seata.demo.order.mapper.OrdersMapper;
import com.weir.seata.demo.order.service.IOrdersService;

import io.seata.spring.annotation.GlobalTransactional;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

	@Autowired
	private StorageApi storageApi;
	@Autowired
	private AccountApi accountApi;
	
	@Override
	@GlobalTransactional(rollbackFor = Exception.class)
	public void saveAll(Orders orders) {
		save(orders);
		storageApi.save(orders.getStorageCode(), orders.getOrderCount());
		accountApi.save(orders.getUserId(), orders.getOrderMoney());
	}
}
