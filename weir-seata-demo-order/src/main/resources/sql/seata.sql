/*
 Navicat Premium Data Transfer

 Source Server         : lo
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : seata

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 05/03/2020 11:56:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for branch_table
-- ----------------------------
DROP TABLE IF EXISTS `branch_table`;
CREATE TABLE `branch_table` (
  `branch_id` bigint NOT NULL,
  `xid` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_id` bigint DEFAULT NULL,
  `resource_group_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `resource_id` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `lock_key` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `branch_type` varchar(8) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `client_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `application_data` varchar(2000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `idx_xid` (`xid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for global_table
-- ----------------------------
DROP TABLE IF EXISTS `global_table`;
CREATE TABLE `global_table` (
  `xid` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `transaction_id` bigint DEFAULT NULL,
  `status` tinyint NOT NULL,
  `application_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transaction_service_group` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transaction_name` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `timeout` int DEFAULT NULL,
  `begin_time` bigint DEFAULT NULL,
  `application_data` varchar(2000) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`xid`),
  KEY `idx_gmt_modified_status` (`gmt_modified`,`status`),
  KEY `idx_transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for lock_table
-- ----------------------------
DROP TABLE IF EXISTS `lock_table`;
CREATE TABLE `lock_table` (
  `row_key` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
  `xid` varchar(96) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `transaction_id` mediumtext COLLATE utf8mb4_general_ci,
  `branch_id` mediumtext COLLATE utf8mb4_general_ci,
  `resource_id` varchar(256) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `table_name` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pk` varchar(36) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`row_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for seata_state_inst
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_inst`;
CREATE TABLE `seata_state_inst` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `machine_inst_id` varchar(46) NOT NULL COMMENT 'state machine instance id',
  `name` varchar(255) NOT NULL COMMENT 'state name',
  `type` varchar(20) DEFAULT NULL COMMENT 'state type',
  `service_name` varchar(255) DEFAULT NULL COMMENT 'service name',
  `service_method` varchar(255) DEFAULT NULL COMMENT 'method name',
  `service_type` varchar(16) DEFAULT NULL COMMENT 'service type',
  `business_key` varchar(48) DEFAULT NULL COMMENT 'business key',
  `state_id_compensated_for` varchar(32) DEFAULT NULL COMMENT 'state compensated for',
  `state_id_retried_for` varchar(32) DEFAULT NULL COMMENT 'state retried for',
  `gmt_started` timestamp NOT NULL COMMENT 'start time',
  `is_for_update` tinyint(1) DEFAULT NULL COMMENT 'is service for update',
  `input_params` text COMMENT 'input parameters',
  `output_params` text COMMENT 'output parameters',
  `status` varchar(2) NOT NULL COMMENT 'status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `excep` blob COMMENT 'exception',
  `gmt_end` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'end time',
  PRIMARY KEY (`id`,`machine_inst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for seata_state_machine_def
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_machine_def`;
CREATE TABLE `seata_state_machine_def` (
  `id` varchar(32) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'name',
  `tenant_id` varchar(32) NOT NULL COMMENT 'tenant id',
  `app_name` varchar(32) NOT NULL COMMENT 'application name',
  `type` varchar(20) DEFAULT NULL COMMENT 'state language type',
  `comment_` varchar(255) DEFAULT NULL COMMENT 'comment',
  `ver` varchar(16) NOT NULL COMMENT 'version',
  `gmt_create` timestamp NOT NULL COMMENT 'create time',
  `status` varchar(2) NOT NULL COMMENT 'status(AC:active|IN:inactive)',
  `content` text COMMENT 'content',
  `recover_strategy` varchar(16) DEFAULT NULL COMMENT 'transaction recover strategy(compensate|retry)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for seata_state_machine_inst
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_machine_inst`;
CREATE TABLE `seata_state_machine_inst` (
  `id` varchar(46) NOT NULL COMMENT 'id',
  `machine_id` varchar(32) NOT NULL COMMENT 'state machine definition id',
  `tenant_id` varchar(32) NOT NULL COMMENT 'tenant id',
  `parent_id` varchar(46) DEFAULT NULL COMMENT 'parent id',
  `gmt_started` timestamp NOT NULL COMMENT 'start time',
  `business_key` varchar(48) DEFAULT NULL COMMENT 'business key',
  `start_params` text COMMENT 'start parameters',
  `gmt_end` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'end time',
  `excep` blob COMMENT 'exception',
  `end_params` text COMMENT 'end parameters',
  `status` varchar(2) DEFAULT NULL COMMENT 'status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `compensation_status` varchar(2) DEFAULT NULL COMMENT 'compensation status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `is_running` tinyint(1) DEFAULT NULL COMMENT 'is running(0 no|1 yes)',
  `gmt_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unikey_buz_tenant` (`business_key`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
