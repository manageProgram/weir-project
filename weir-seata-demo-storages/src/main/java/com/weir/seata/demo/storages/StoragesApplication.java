package com.weir.seata.demo.storages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class StoragesApplication {
    public static void main(String[] args) {
        SpringApplication.run(StoragesApplication.class, args);
    }
}
