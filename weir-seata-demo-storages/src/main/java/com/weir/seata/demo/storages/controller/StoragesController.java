package com.weir.seata.demo.storages.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.weir.seata.demo.storages.entity.Storages;
import com.weir.seata.demo.storages.service.IStoragesService;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@RestController
@RequestMapping("/storages/storages")
@Slf4j
public class StoragesController {

	@Autowired
	private IStoragesService storagesService;
	
	@GetMapping("/save/{storageCode}/{storageCount}")
	public void save(@PathVariable String storageCode,@PathVariable Integer storageCount) {
		log.info("----库存操作-开始----");
		Storages storages = storagesService.getOne(new QueryWrapper<Storages>().eq("storage_code", storageCode));
		if (storages == null) {
			return ;
		}
		storages.setStorageCount(storages.getStorageCount() - storageCount);
		boolean updateById = storagesService.updateById(storages);
		log.info("----库存操作-完成----" + updateById);
		
	}
}

