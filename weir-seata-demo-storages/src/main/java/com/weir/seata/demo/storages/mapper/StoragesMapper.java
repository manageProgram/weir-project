package com.weir.seata.demo.storages.mapper;

import com.weir.seata.demo.storages.entity.Storages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface StoragesMapper extends BaseMapper<Storages> {

}
