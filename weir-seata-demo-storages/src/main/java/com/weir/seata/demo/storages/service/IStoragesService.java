package com.weir.seata.demo.storages.service;

import com.weir.seata.demo.storages.entity.Storages;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
public interface IStoragesService extends IService<Storages> {

}
