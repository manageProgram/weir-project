package com.weir.seata.demo.storages.service.impl;

import com.weir.seata.demo.storages.entity.Storages;
import com.weir.seata.demo.storages.mapper.StoragesMapper;
import com.weir.seata.demo.storages.service.IStoragesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2020-02-28
 */
@Service
public class StoragesServiceImpl extends ServiceImpl<StoragesMapper, Storages> implements IStoragesService {

}
